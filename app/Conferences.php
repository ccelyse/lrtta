<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conferences extends Model
{
    protected $table = "conferences";
    protected $fillable = ['id','conference_title','conference_image_name'];
}
