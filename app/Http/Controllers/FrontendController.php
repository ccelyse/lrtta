<?php

namespace App\Http\Controllers;

use App\ActivityDetails;
use App\ActivitySummary;
use App\CompanyCategory;
use App\Conferences;
use App\DomesticPackages;
use App\JoinMember;
use App\MemberPhotoLibrary;
use App\Packages;
use App\PhotoCategories;
use App\Post;
use App\SubCompany;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FrontendController extends Controller
{
    public function Home(){
        return view('welcome');
    }
    public function AboutUs(){
        return view('Aboutus');
    }
    public function Team(){
        return view('Team');
    }
    public function AboutRwanda(){
        return view('AboutRwanda');
    }
    public function GeneralTips(){
        return view('GeneralTips');
    }
    public function VisaInformation(){
        return view('VisaInformation');
    }
    public function JoinUs(){
        return view('JoinUs');
    }
    public function DomesticPackages(){
        $imagename = "center.jpeg";
        $data = DomesticPackages::all();
        return view('DomesticPackages')->with(['data'=>$data,'imagename'=>$imagename]);
    }
    public function DomesticPackagesV1(){
        $imagename = "center.jpeg";
        $data = DomesticPackages::all();
        return view('DomesticPackagesV1')->with(['data'=>$data,'imagename'=>$imagename]);
    }
    public function DomesticPackagesByCategory(Request $request){
        $category = $request['category'];
        if($category == "All"){
            $data_ = DomesticPackages::all();
            if(0 == count($data_)){
                return response()->json([
                    'response_message' => "no packages yet",
                    'response_status' =>400
                ]);
            }else{
                return response()->json($data_);
            }
        }else{
            $data_ = DomesticPackages::where('package_category',$category)->get();
            if(0 == count($data_)){
                return response()->json([
                    'response_message' => "no packages yet",
                    'response_status' =>400
                ]);
            }else{
                return response()->json($data_);
            }
        }


    }
    public function DomesticPackagesById(Request $request){
        $id = $request['id'];
        $data_ = DomesticPackages::where('id',$id)->get();
        if(0 == count($data_)){
            return response()->json([
                'response_message' => "no packages yet",
                'response_status' =>400
            ]);
        }else{
            return response()->json($data_);
        }
    }
    public function MoreEvents(){
        $imagename = "center.jpeg";
        $data = ActivitySummary::whereIn('id',['3','4','7','9','10','12','13','14','15','16'])->get();
        return view('MoreEvents')->with(['data'=>$data,'imagename'=>$imagename]);
    }
    public function MoreEventsM(){
        $imagename = "ikazekigali.jpg";
        $data = ActivitySummary::whereIn('id',['17','18','19','20','21','22','23','24','25','26','27'])->get();
        return view('MoreEvents')->with(['data'=>$data,'imagename'=>$imagename]);
    }
    public function MoreEventsGSM(){
        $imagename = "gsm.jpg";
        $data = ActivitySummary::whereIn('id',['28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','17','19','20','21'])->get();
        return view('MoreEvents')->with(['data'=>$data,'imagename'=>$imagename]);
    }

    public function ConferenceTourPackage(){
        $data = ActivitySummary::all();
        $listconference = Conferences::all();
        return view('ConferenceTourPackage')->with(['data'=>$data,'listconference'=>$listconference]);
    }
    public function ConferencePackages(Request $request){
        $listconference = Conferences::where('id',$request['id'])->get();
        $listpackages = Packages::where('packages.conference_id',$request['id'])
            ->join('conferences', 'conferences.id', '=', 'packages.conference_id')
            ->select('packages.*', 'conferences.conference_title')
            ->get();
        return view('ConferencePackages')->with(['listpackages'=>$listpackages,'listconference'=>$listconference]);

    }
    public function MoreTourPackage(Request $request){
        $listconference = Conferences::where('id',$request['id'])->get();
        $listpackages = Packages::where('packages.conference_id',$request['id'])
            ->join('conferences', 'conferences.id', '=', 'packages.conference_id')
            ->select('packages.*', 'conferences.conference_title')
            ->get();
        return view('MoreTourPackageMoreEvents')->with(['listpackages'=>$listpackages,'listconference'=>$listconference]);

    }
    public function TourPackage(Request $request){
        $id = $request['id'];
        $summary = ActivitySummary::where('id',$id)->get();
        $details = ActivityDetails::where('activitysummary_id',$id)->get();
        return view('TourPackage')->with(['summary'=>$summary,'details'=>$details]);
    }

    public function VisitRwanda()
    {
//        $curl = curl_init();
//
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => "http://rha.rw/api/AttractionAPI",
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => "",
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 30,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => "GET",
//            CURLOPT_POSTFIELDS => "",
//            CURLOPT_HTTPHEADER => array(
//                "Postman-Token: d098a4a0-5bcf-4e61-bd5e-eecb1d191054",
//                "cache-control: no-cache"
//            ),
//        ));
//
//        $response = curl_exec($curl);
//        $err = curl_error($curl);
//        $data = json_decode($response);
//
////        dd($data);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://visiteastafrica.travel/api/DestinationApi",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Postman-Token: d1901bf9-8df0-47bf-811c-186bed1bc29b",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $data = json_decode($response);

//        curl_close($curl);

        return view('VisitRwanda')->with(['data' => $data]);

    }
    public function BecomeMember(){
//        $listcompany = CompanyCategory::all();
        return view('BecomeMember');
    }

    public function JoinMembers(Request $request){
        $all = $request->all();
//        dd($all);

        $messages = [
            'fileToUpload' => 'The file to upload must be in PDF format',
            'attachyourcv' => 'The file to upload must be in PDF format',
            'attachyourdiplomas' => 'The file to upload must be in PDF format',
            'drivinglicense' => 'The file to upload must be in PDF format',
        ];
        $request->validate([
            'attachyourcv'   => 'mimes:pdf',
            'attachyourdiplomas'   => 'mimes:pdf',
            'drivinglicense'   => 'mimes:pdf',

        ],$messages);

        $firstname = $request['firstname'];
        $lastname = $request['lastname'];

        $JoinM = new JoinMember();

        $attachyourpassport = $request->file('attachyourpassport');
        $attachyourcv = $request->file('attachyourcv');
        $attachyourdiplomas = $request->file('attachyourdiplomas');
        $drivinglicense = $request->file('drivinglicense');

        $JoinM->typeofmembership = $request['typeofmembership'];
        $JoinM->chamber = $request['chamber'];
        $JoinM->firstname = $request['firstname'];
        $JoinM->lastname = $request['lastname'];
        $JoinM->tinnumber = $request['tinnumber'];
        $JoinM->telephonenumber = $request['telephonenumber'];
        $JoinM->email = $request['email'];
        $JoinM->website = $request['website'];
        $JoinM->age = $request['age'];
        $JoinM->nationality = $request['nationality'];
        $JoinM->province = $request['province'];
        $JoinM->district = $request['district'];
        $JoinM->sector = $request['sector'];
        $JoinM->cell = $request['cell'];
        $JoinM->workingexperience = $request['workingexperience'];
        $JoinM->educationlevel = $request['educationlevel'];
        $JoinM->areofinterest = $request['areofinterest'];
        $JoinM->export = $request['export'];
        $Memberstatus = 'No Status yet';
        $JoinM->Memberstatus = $Memberstatus;
        $JoinM->user_id = '1';

        $JoinM->attachyourpassport = $attachyourpassport->getClientOriginalName();
        $JoinM->attachyourcv = $attachyourcv->getClientOriginalName();
        $JoinM->attachyourdiplomas = $attachyourdiplomas->getClientOriginalName();
        $JoinM->drivinglicense = $drivinglicense->getClientOriginalName();

        $attachyourpassport_ = $attachyourpassport->getClientOriginalName();
        $attachyourcv_ = $attachyourcv->getClientOriginalName();
        $attachyourdiplomas_ = $attachyourdiplomas->getClientOriginalName();
        $drivinglicense_ = $drivinglicense->getClientOriginalName();

        $destinationPathpassport = public_path('/passport');
        $destinationPathcv = public_path('/cv');
        $destinationPathdiplomas = public_path('/diplomas');
        $destinationPathdrivinglicense = public_path('/drivinglicense');

        $attachyourpassport->move($destinationPathpassport, $attachyourpassport_);
        $attachyourcv->move($destinationPathcv, $attachyourcv_);
        $attachyourdiplomas->move($destinationPathdiplomas, $attachyourdiplomas_);
        $drivinglicense->move($destinationPathdrivinglicense, $drivinglicense_);

        $JoinM->save();
        return view('Thankyou')->with(['firstname'=>$firstname,'lastname'=>$lastname]);
    }
    public function BusinessLicensing(){
        return view('BusinessLicensing');
    }
    public function News(){
        $newpost = Post::orderBy('created_at','desc')->get();
        return view('News')->with(['newpost'=>$newpost]);
    }
    public function NewsReadMore(Request $request){
        $id = $request['id'];
        $newpost = Post::where('id',$id)->get();
        return view('NewsReadMore')->with(['newpost'=>$newpost]);
    }
    public function Publications(){
        return view('Publications');
    }
    public function ContactUs(){
        return view('ContactUs');
    }
    public function Thankyou(){
        return view('Thankyou');
    }
    public function Login(){
        return view('backend.Login');
    }
    public function MemberDirectory(){
        $listcat = JoinMember::where('approval','Approved')->get();
        return view('MemberDirectory')->with(['listcat'=>$listcat]);
    }
    public function Webmail(){
        return redirect('https://mail.rha.rw/');
    }
    public function StockPicture(){
        $listcat = PhotoCategories::all();
        return view('StockPicture')->with(['listcat'=>$listcat]);
    }
    public function PhotoLibraryViewMore(Request $request){
        $id = $request['category'];
        $listcat = PhotoCategories::where('categoryname',$id)->get();
        $findpicture= MemberPhotoLibrary::where('photocategories',$id)->get();
        return view('PhotoLibraryViewMore')->with(['findpicture'=>$findpicture,'listcat'=>$listcat]);
    }
    public function BookNow(Request $request){
        $all = $request->all();
//        dd($all);

        $name = $request['name'];
        $email = $request['fromemail'];
        $toemail = $request['toemail'];
        $message = $request['message'];
        $company_name = $request['company_name'];
        $subjectmail = $request['subject'];

        $data = array(
            'namemail'=>$company_name,
            'messagemail'=>$message,
        );


        Mail::send('backend.mail', $data, function($message) use ($email, $name,$subjectmail,$toemail,$company_name)
        {
            $message->from($email, $name);
            $message->to($toemail, $company_name)->cc('rttarwanda@gmail.com')->subject($subjectmail);
        });

        return view('Thankyou')->with(['name'=>$name]);
    }

//    public function ThankYou(){
//        return view('ThankYou');
//    }


//    public function HotelCategory(Request $request)
//    {
//        $cate = $request['hotelcategory'];
////        $cate = 'Accomodation';
////        $cate = 'Bar';
//
//
//        if ($cate == 'Accomodation') {
//            $checksub = SubCompany::where('companycategory', $cate)->get();
////            echo "<option value=\"\" selected>Select Sub Category</option>";
////            echo "<label for=\"billing_country\" class=\"\">Company Sub Category</label><select class=\"country_to_state country_select billing_country\" name=\"hotelcategory\" id=\"hotelcategory\" required><option value=\"\">$datas->subcompanycategory</option></select>";
////            foreach ($checksub as $datas) {
////                echo "<option value=\"$datas->subcompanycategory\">$datas->subcompanycategory</option>";
////            }
//            echo "<label for=\"billing_last_name\" class=\"\">Company Sub Category</label><select class=\"form-control\" id=\"basicSelect\" name=\"subcompanycategory\" style='width:60% !important' onChange=\"getSubCat(this.value);\"><option value=\"Hotel\">Hotel</option><option value=\"Apartment\">Apartment</option><option value=\"Villa\">Villa</option><option value=\"Motel\">Motel</option><option value=\"Cottage\">Cottage</option><option value=\"Logde\">Logde</option><option value=\"Tented camp\">Tented camp</option></select>";
//
//        }elseif($cate == 'Restaurant' || $cate == 'Nightclub'|| $cate == 'Coffee shop' || $cate == 'Bar'){
////            echo "<option value=\"1 star\" selected>1 star</option>";
////            echo "<option value=\"2 star\" selected>2 star</option>";
////            echo "<option value=\"3 star\" selected>3 star</option>";
////            echo "<option value=\"4 star\" selected>4 star</option>";
////            echo "<option value=\"5 star\" selected>5 star</option>";
////
//            echo "<label for=\"billing_last_name\" class=\"\">Seating Capacity (Eg:50)</label><input type=\"text\" class=\"input-text\" name=\"seatingcapacity\" id=\"billing_last_name\" placeholder=\"\"  required/>";
////            echo "<input type=\"text\" class=\"input-text\" name=\"Seatingcapacity\" id=\"billing_last_name\" placeholder=''>";
//        }
//
//    }
//    public function SubCategory(Request $request){
//        $subcompanycategory = $request['subcompanycategory'];
//        if($subcompanycategory == 'Hotel' || $subcompanycategory == 'Apartment'|| $subcompanycategory == 'Logde'){
//            echo "<label for=\"billing_last_name\" class=\"\">Grading</label><select class=\"form-control\" id=\"basicSelect\" name=\"subcompanycategory\" style='width:60% !important;margin-bottom: 15px;' onChange=\"getCity(this.value);\"><option value=\"Hotel\">1 star</option><option value=\"Apartment\">2 star</option><option value=\"Villa\">3 star</option><option value=\"Motel\">4 star</option><option value=\"Cottage\">5 star</option></select>";
//            echo"<label for=\"billing_last_name\" class=\"\">Number of Rooms</label><input type=\"text\" class=\"input-text\" name=\"numberofrooms\" id=\"billing_last_name\" placeholder=\"\" />";
//        }
//
//    }

}
