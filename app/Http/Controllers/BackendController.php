<?php

namespace App\Http\Controllers;

use AfricasTalking\SDK\AfricasTalking;
use App\ActivityDetails;
use App\ActivitySummary;
use App\Attractions;
use App\CompanyCategory;
use App\Conferences;
use App\Countries;
use App\DomesticPackages;
use App\Hireaguide;
use App\JoinMember;
use App\MemberPhotoLibrary;
use App\MemberPhotoLibraryCategories;
use App\Packages;
use App\PhotoCategories;
use App\Post;
use App\SubCompany;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
require_once('./Class_MR_SMS_API.php');
class BackendController extends Controller
{

    public function AccountList(){
        $listaccount = User::where('role','0')->get();
        return view('backend.AccountList')->with(['listaccount'=>$listaccount]);
    }
    public function Dashboard(){
        /*$now = Carbon::now();
        $nowdate = Carbon::now();
        $nowyear = $now->addYear(2);


        $day = strtotime($now->toDateString());

        $renewdate = JoinMember::whereBetween('expiration_date',[$nowdate,$nowyear])->get();

        foreach ($renewdate as $datas){
            $date_renew = $datas->expiration_date;
            $dateonly = strtotime($date_renew);
//            echo "<p>$dateonly</p>";
            $nowexp = time();
            $datediff = $dateonly - $nowexp;
            $daysexpiration = round($datediff / (60 * 60 * 24));

            if ($daysexpiration >= 30){

            }
        }*/


        $jan = new Carbon('first day of January');

        $jan_end = new Carbon('last day of January');

        $feb = new Carbon('first day of February');
        $feb_end = new Carbon('last day of February');

        $march = new Carbon('first day of March');
        $march_end = new Carbon('last day of March');

        $april = new Carbon('first day of April');
        $april_end = new Carbon('last day of April');

        $may= new Carbon('first day of May');
        $may_end = new Carbon('last day of May');

        $june = new Carbon('first day of June');
        $june_end = new Carbon('last day of June');

        $july = new Carbon('first day of July');
        $july_end = new Carbon('last day of July');

        $august = new Carbon('first day of August');
        $august_end = new Carbon('last day of August');

        $septmber = new Carbon('first day of September');
        $septmber_end = new Carbon('last day of September');

        $october = new Carbon('first day of October');
        $october_end = new Carbon('last day of October');

        $november = new Carbon('first day of November');
        $november_end = new Carbon('last day of November');

        $december = new Carbon('first day of December');
        $december_end = new Carbon('last day of December');


        /**
         *
         *  application member
         */

        $appmember = JoinMember::select(DB::raw('count(id) as appmember'))->value('appmember');

        $approvedmember = JoinMember::select(DB::raw('count(id) as approvedmember'))
            ->where('approval','Approved')
            ->value('approvedmember');

        $deniedmember = JoinMember::select(DB::raw('count(id) as deniedmember'))
            ->where('approval','Denied')
            ->value('deniedmember');

        $guiderequest = Hireaguide::select(DB::raw('count(id) as guiderequest'))->value('guiderequest');

        /**
         *
         * MEmber application  (JANUARY)
         */

        $janmember = JoinMember::select(DB::raw('count(id) as janmember'))
            ->whereBetween('created_at',[$jan->format('Y/m/d'),$jan_end->format('Y/m/d')])
            ->value('janmember');

        /**
         *
         * MEmber application  (Feb)
         */

        $febmember = JoinMember::select(DB::raw('count(id) as febmember'))
            ->whereBetween('created_at',[$feb->format('Y/m/d'),$feb_end->format('Y/m/d')])
            ->value('febmember');

        /**
         *
         * MEmber application  (mar)
         */

        $marmember = JoinMember::select(DB::raw('count(id) as marmember'))
            ->whereBetween('created_at',[$march->format('Y/m/d'),$march_end->format('Y/m/d')])
            ->value('marmember');
        /**
         *
         * MEmber application  (apr)
         */

        $aprmember = JoinMember::select(DB::raw('count(id) as aprmember'))
            ->whereBetween('created_at',[$april->format('Y/m/d'),$april_end->format('Y/m/d')])
            ->value('aprmember');

        /**
         *
         * MEmber application  (may)
         */

        $maymember = JoinMember::select(DB::raw('count(id) as maymember'))
            ->whereBetween('created_at',[$may->format('Y/m/d'),$may_end->format('Y/m/d')])
            ->value('maymember');

        /**
         *
         * MEmber application  (jun)
         */

        $junmember = JoinMember::select(DB::raw('count(id) as junmember'))
            ->whereBetween('created_at',[$june->format('Y/m/d'),$june_end->format('Y/m/d')])
            ->value('junmember');

        /**
         *
         * MEmber application  (jul)
         */

        $julmember = JoinMember::select(DB::raw('count(id) as julmember'))
            ->whereBetween('created_at',[$july->format('Y/m/d'),$july_end->format('Y/m/d')])
            ->value('julmember');

        /**
         *
         * MEmber application  (aug)
         */

        $augmember = JoinMember::select(DB::raw('count(id) as augmember'))
            ->whereBetween('created_at',[$august->format('Y/m/d'),$august_end->format('Y/m/d')])
            ->value('augmember');

        /**
         *
         * MEmber application  (sep)
         */

        $sepmember = JoinMember::select(DB::raw('count(id) as sepmember'))
            ->whereBetween('created_at',[$septmber->format('Y/m/d'),$septmber_end->format('Y/m/d')])
            ->value('sepmember');


        /**
         *
         * MEmber application  (oct)
         */


        $octmember = JoinMember::select(DB::raw('count(id) as octmember'))
            ->whereBetween('created_at',[$october->format('Y/m/d'),$october_end->format('Y/m/d')])
            ->value('octmember');

        /**
         *
         * MEmber application  (nov)
         */


        $novmember = JoinMember::select(DB::raw('count(id) as novmember'))
            ->whereBetween('created_at',[$november->format('Y/m/d'),$november_end->format('Y/m/d')])
            ->value('novmember');

        /**
         *
         * MEmber application  (dec)
         */

        $decmember = JoinMember::select(DB::raw('count(id) as decmember'))
            ->whereBetween('created_at',[$december->format('Y/m/d'),$december_end->format('Y/m/d')])
            ->value('decmember');

        return view('backend.Dashboard')->with(['janmember'=>$janmember,'febmember'=>$febmember,
            'marmember'=>$marmember,'aprmember'=>$aprmember,'maymember'=>$maymember,'junmember'=>$junmember,'julmember'=>$julmember,
            'augmember'=>$augmember,'sepmember'=>$sepmember,'octmember'=>$octmember,'novmember'=>$novmember,'decmember'=>$decmember,
            'appmember'=>$appmember,'approvedmember'=>$approvedmember,'deniedmember'=>$deniedmember,'guiderequest'=>$guiderequest]);
    }
    public function CreateAccount(){
        return view('backend.CreateAccount');
    }
    public function CreateAccount_(Request $request){
        $all  = $request->all();

        $request->validate([
            'name' => 'required|string:posts|max:255',
            'email' => 'required|string|email|max:255|unique:users','exists:connection.user,email',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $register = new User();
        $register->name = $request['name'];
        $register->email = $request['email'];
        $register->password = bcrypt($request['password']);
        $register->role = '0';
        $register->joinmember_id = '0';
        $register->save();

        return redirect()->route('backend.AccountList')->with('Success','You have successfully Created Your Account');
    }
    public function UpdateAccountInfo(Request $request){
        $all = $request->all();
        $password = $request['password'];
        $id = $request['id'];
        if($password == null){
           $update = User::find($id);
            $update->name = $request['name'];
            $update->email = $request['email'];
            $update->save();
            return back()->with('success','you have successfully updated account information');
        }else{
            $update = User::find($id);
            $update->name = $request['name'];
            $update->email = $request['email'];
            $update->password = bcrypt($request['password']);;
            $update->save();
            return back()->with('success','you have successfully updated account information');
        }
    }

    public function SignIn_(Request $request){

        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {

            $user_get_role = User::where('email', $request['email'])->value('role');

            switch ($user_get_role) {
                case "0":
                    return redirect()->intended('/Dashboard');
                    break;
                case "1":
                    return redirect()->intended('/MembersProfile');
                    break;
                default:
                    return back();
                    break;
            }

        }
        else{
            return back()->with('success','your email or your password is not matching');
        }
    }
    public function getLogout(){
        Auth::logout();
        return redirect()->route('welcome');
    }

    public function ListOfMembers(){
        $listmembers = JoinMember::orderBy('created_at', 'desc')->where('Memberstatus','No Status yet')->orWhere('Memberstatus', 'Approved')
            ->join('users', 'users.id', '=', 'JoinMember.user_id')
            ->select('JoinMember.*','users.name')
            ->get();
        return view('backend.ListOfMembers')->with(['listmembers'=>$listmembers]);

    }
    public function FilterMembers(){

        $listmembers = JoinMember::orderBy('created_at', 'desc')->where('Memberstatus','No Status yet')->orWhere('Memberstatus', 'Approved')
            ->join('users', 'users.id', '=', 'JoinMember.user_id')
            ->select('JoinMember.*','users.name')
            ->get();
        return view('backend.FilterMembers')->with(['listmembers'=>$listmembers]);
    }
    public function FilterMembers_(Request $request){
        $all = $request->all();
        $district = $request['district'];
        $filter = JoinMember::where('district', 'LIKE', '%'.$district.'%')->get();
        return view('backend.FilterMembers')->with(['filter'=>$filter]);

    }

    public function ApproveMember(Request $request){

        $id = $request['MemberId'];
        $status = $request['Status'];
        $currentMonth = Carbon::now()->format('m');
        $code = 'RSGA'. mt_rand(10, 99). $currentMonth;
        $date_now = Carbon::now();
        $dateafteryear = Carbon::now()->addYear();

        $getmemberinfo = JoinMember::where('id',$id)->get();
        foreach ($getmemberinfo as $info){
            $useremail = $info->email;
            $userfirstname = $info->firstname;
            $userlastname = $info->lastname;
            $userrole = '1';
        }

        switch ($status) {
            case "Approved":
                $update_member = JoinMember::find($id);
                $update_member->approval ='Approved';
                $update_member->Memberstatus ='Approved';
                $update_member->approved_date =$date_now;
                $update_member->expiration_date =$dateafteryear;
                $update_member->identification =$code;
                $update_member->save();

                $createauser = new user();
                $createauser->name = $userfirstname.' '.$userlastname;
                $createauser->email = $useremail;
                $createauser->password = bcrypt($code);
                $createauser->role = $userrole;
                $createauser->joinmember_id = $id;
                $createauser->save();

                break;

            case "Denied":
                $update_member = JoinMember::find($id);
                $update_member->approval ='Denied';
                $update_member->Memberstatus ='Denied';
                $update_member->identification ='';
                $update_member->approved_date =NULL;
                $update_member->expiration_date =NULL;
                $update_member->save();
                break;

        }
        return back()->with('success','You have successfully updated members list');
    }
    public function NotifyMember(Request $request){
        $all = $request->all();
        $rsgamessage = $request['limitedtextarea'];
        $phonenumber = $request['phonenumber'];
        $intnumber = '25'.$phonenumber;
        $api_key = 'ZW93RklkQ2FHaklNc0trSWxLSG4=';
        $from = 'RSGA';
        $destination = $intnumber;
        $action='send-sms';
        $url = 'https://mistasms.com/sms/api';
        $sms = $rsgamessage;
        $unicode = 1; //For Plain Message
        $unicode = 0; //For Unicode Message
        $sms_body = array(
            'api_key' => $api_key,
            'to' => $destination,
            'from' => $from,
            'sms' => $sms,
            'unicode' => $unicode,
        );
        $client = new \MRSMSAPI();
        $response = $client->send_sms($sms_body, $url);
        $response=json_decode($response);
//        dd($response);
        if($response->code == 'ok'){
            return back()->with('success','you have successfully sent your message');
        }else{
            return back()->with('danger','your message is not successfully sent');
        }
    }

    public function EditJoinMember(Request $request){
        $id = $request['id'];

        $edit = JoinMember::where('id',$id)->get();

        return view('backend.EditJoinMember')->with(['edit'=>$edit]);
    }
    public function EditJoinMember_(Request $request){
        $id  = $request['id'];
        $all = $request->all();
        $Memberstatus = 'No Status yet';
        $image = $request->file('fileToUpload');
        $user_id = \Auth::user()->id;


        if ($request->hasFile('fileToUpload')) {
            $EditJoinM = JoinMember::find($id);
            $EditJoinM->typeofmembership = $request['typeofmembership'];
            $EditJoinM->chamber = $request['chamber'];
            $EditJoinM->companyname = $request['companyname'];
            $EditJoinM->companycode = $request['companycode'];
            $EditJoinM->hotelcategory = $request['hotelcategory'];
            $EditJoinM->numberofrooms = $request['numberofrooms'];
            $EditJoinM->gender = $request['gender'];
            $EditJoinM->phonenumber = $request['phonenumber'];
            $EditJoinM->pobx = $request['pobx'];
            $EditJoinM->email = $request['email'];
            $EditJoinM->website = $request['website'];
            $EditJoinM->businessaddress = $request['businessaddress'];
            $EditJoinM->buildingname = $request['buildingname'];
            $EditJoinM->businessarea = $request['businessarea'];
            $EditJoinM->province = $request['province'];
            $EditJoinM->district = $request['district'];
            $EditJoinM->sector = $request['sector'];
            $EditJoinM->cell = $request['cell'];
            $EditJoinM->companytype = $request['companytype'];
            $EditJoinM->ownership = $request['ownership'];
            $EditJoinM->businessactivity = $request['businessactivity'];
            $EditJoinM->export = $request['export'];
            $EditJoinM->numberofemployees = $request['numberofemployees'];
            $EditJoinM->numberofemployeesapart = $request['numberofemployeesapart'];
            $EditJoinM->user_id = $user_id;
            $EditJoinM->documentname = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $input['fileToUpload'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/rdbcertificate');
            $image->move($destinationPath, $imagename);
            $EditJoinM->save();
            return redirect()->route('backend.ListOfMembers')->with('update','You have successfully updated Member Information');
        }else{
            $EditJoinM = JoinMember::find($id);
            $EditJoinM->typeofmembership = $request['typeofmembership'];
            $EditJoinM->chamber = $request['chamber'];
            $EditJoinM->companyname = $request['companyname'];
            $EditJoinM->companycode = $request['companycode'];
            $EditJoinM->hotelcategory = $request['hotelcategory'];
            $EditJoinM->numberofrooms = $request['numberofrooms'];
            $EditJoinM->gender = $request['gender'];
            $EditJoinM->phonenumber = $request['phonenumber'];
            $EditJoinM->pobx = $request['pobx'];
            $EditJoinM->email = $request['email'];
            $EditJoinM->website = $request['website'];
            $EditJoinM->businessaddress = $request['businessaddress'];
            $EditJoinM->buildingname = $request['buildingname'];
            $EditJoinM->businessarea = $request['businessarea'];
            $EditJoinM->province = $request['province'];
            $EditJoinM->district = $request['district'];
            $EditJoinM->sector = $request['sector'];
            $EditJoinM->cell = $request['cell'];
            $EditJoinM->companytype = $request['companytype'];
            $EditJoinM->ownership = $request['ownership'];
            $EditJoinM->businessactivity = $request['businessactivity'];
            $EditJoinM->export = $request['export'];
            $EditJoinM->numberofemployees = $request['numberofemployees'];
            $EditJoinM->numberofemployeesapart = $request['numberofemployeesapart'];
            $EditJoinM->user_id = $user_id;
            $EditJoinM->save();
            return redirect()->route('backend.ListOfMembers')->with('update','You have successfully updated Member Information');
        }
    }
    public function AddAttractions_(Request $request){
        $all = $request->all();
//        dd($all);
//        $request->validate([
//            'fileToUpload'   => 'mimes:pdf'
//        ]);

        $saveattractions = new Attractions();

        $image = $request->file('fileToUpload');

        $saveattractions->attraction_province = $request['attraction_province'];
        $saveattractions->attraction_name = $request['attraction_name'];
        $saveattractions->attraction_indetails = $request['attraction_indetails'];

        $saveattractions->attraction_image = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $input['fileToUpload'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/attractions');
        $image->move($destinationPath, $imagename);

        $saveattractions->save();

        return back()->with('success','you have created a new attraction');
    }
    public function AddNews(){
        return view('backend.AddNews');
    }
    public function AddNews_(Request $request){
        $all = $request->all();
        $addnews = new Post();
        $addnews->news_title = $request['news_title'];
        $addnews->news_details = $request['news_details'];
        $addnews->newsdescription = $request['newsdescription'];
        $addnews->save();
        return back()->with('success','you have created a new post');
    }
    public function NewsList(){
        $listnews = Post::all();
        return view('backend.NewsList')->with(['listnews'=>$listnews]);
    }
    public function EditNews(Request $request){
        $id = $request['id'];
        $editnews = Post::where('id',$id)->get();

        return view('backend.EditNews')->with(['editnews'=>$editnews]);
    }
    public function EditNews_(Request $request){
        $id = $request['id'];
        $editnews = Post::find($id);
        $editnews->news_title = $request['news_title'];
        $editnews->news_details = $request['news_details'];
        $editnews->newsdescription = $request['newsdescription'];
        $editnews->save();
        return redirect()->route('backend.NewsList')->with('success','You have successful updated a Post');


    }
    public function DeleteNews(Request $request){
        $id = $request['id'];
        $deletebrand = Post::find($id);
        $deletebrand->delete();
        return back()->with('success','you successfully deleted a post');
    }
    public function VisitRwanda()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => "9003",
            CURLOPT_URL => "http://127.0.0.1:9003/api/AttractionAPI",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Postman-Token: d9d80d4d-87e4-46fb-b2a5-87239098a093",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $data = json_decode($response);

        return view('VisitRwanda')->with(['data' => $data]);

    }
    public function AttractionsMore(Request $request){
        $id = $request['id'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://visiteastafrica.travel/api/DestinationApiMore?id=$id",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Postman-Token: d1901bf9-8df0-47bf-811c-186bed1bc29b",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $datas = json_decode($response);
//        dd($datas);


//        $curl = curl_init();


        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://visiteastafrica.travel/api/ViewDestinationPlace?id=$id",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Postman-Token: 77f27011-ffbd-4b52-b6f6-fe7dab5afeba",
                "cache-control: no-cache"
            ),
        ));

        $responsess = curl_exec($curl);
        $errs = curl_error($curl);
        $things = json_decode($responsess);

//        dd($things);

        return view('AttractionsMore')->with(['datas'=>$datas,'things'=>$things]);

    }
    public function Hireaguide(Request $request){

        $hireaguide = new Hireaguide();
        $hireaguide->attractionid = $request['attractionid'];
        $hireaguide->name = $request['name'];
        $hireaguide->email = $request['email'];
        $hireaguide->contactnumber = $request['contactnumber'];
        $hireaguide->country = $request['country'];
        $hireaguide->totaldults = $request['totaldults'];
        $hireaguide->totalchildren = $request['totalchildren'];
        $hireaguide->message = $request['message'];

        $hireaguide->save();

        return back()->with('success','Thank you for requesting a guide! we will get back to you with 24 hours');

    }
    public function GuidesList(){
        $listguides = Hireaguide::all();
        return view('backend.GuidesRequest')->with(['listguides'=>$listguides]);
    }
    public function BankSlip(Request $request){
        $id = $request['id'];
        return view('backend.BankSlip')->with(['id'=>$id]);
    }
    public function AddBankSlip(Request $request){
        $id  = $request['id'];
        return view('backend.AddBankSlip')->with(['id'=>$id]);
    }
    public function BankSlip_(Request $request){
        $all = $request->all();
        $id = $request['id'];
        $addbankslip = JoinMember::find($id);
        $image = $request->file('fileToUpload');
        $addbankslip->bankslip = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $input['fileToUpload'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/bankslip');
        $image->move($destinationPath, $imagename);
        $addbankslip->save();
        return redirect()->route('backend.ListOfMembers')->with('success','you have successfully added bank slip');
    }
    public function SendSMS(){
        $listnumber = JoinMember::where('Memberstatus','Approved')->get();
        return view ('backend.SendSMS')->with(['listnumber'=>$listnumber]);
    }
    public function SendSMS_(Request $request){
        $all = $request->all();
        dd($all);
        $rsgamessage = $request['limitedtextarea'];
        $phonenumber = $request['phonenumber'];
        $manyphonenumber = $request['manyphonenumber'];
        dd($manyphonenumber);
        $intnumber = '25'.$phonenumber;
//        $intmanyphonenumber = '25'.$manyphonenumber;
        if($manyphonenumber == null){
            return back()->with('danger','you did not select phone number');
        }else{
            if($phonenumber == null){

                foreach ($manyphonenumber as $key => $manyphonenumber) {

//                    $phonenumber_ = $manyphonenumber[$key];
                    $api_key = 'Y2NlbHlzZTE6QWthcmFieWUxMjM=';
                    $from = 'RSGA';
                    $destination = $request['manyphonenumber'][$key];
                    $action='send-sms';
                    $url = 'https://mistasms.com/sms/api';
                    $sms = $rsgamessage;
                    $unicode = 1; //For Plain Message
                    $unicode = 0; //For Unicode Message
                    $sms_body = array(
                        'api_key' => $api_key,
                        'to' => $destination,
                        'from' => $from,
                        'sms' => $sms,
                        'unicode' => $unicode,
                    );
                    $client = new \MRSMSAPI();
                    $response = $client->send_sms($sms_body, $url);
                    $response=json_decode($response);
//                    dd($response);

                }

                if($response->code == 'ok'){
                    return back()->with('success','you have successfully sent your message');
                }else{
                    return back()->with('danger','you sms is not successfully sent');
                }
            }else{
                $api_key = 'Y2NlbHlzZTE6QWthcmFieWUxMjM=';
                $from = 'RSGA';
                $destination = $intnumber;
                $action='send-sms';
                $url = 'https://mistasms.com/sms/api';
                $sms = $rsgamessage;
                $unicode = 1; //For Plain Message
                $unicode = 0; //For Unicode Message
                $sms_body = array(
                    'api_key' => $api_key,
                    'to' => $destination,
                    'from' => $from,
                    'sms' => $sms,
                    'unicode' => $unicode,
                );
                $client = new \MRSMSAPI();
                $response = $client->send_sms($sms_body, $url);
                $response=json_decode($response);
                if($response->code == 'ok'){
                    return back()->with('success','you have successfully sent your message');
                }else{
                    return back()->with('danger','you have is not successfully sent');
                }
            }
        }

    }
    public function SendEmail(){
        $listnumber = JoinMember::where('approval','Approved')->get();
        return view ('backend.SendEmail')->with(['listnumber'=>$listnumber]);
    }
    public function SendEmail_(Request $request){
        $all = $request->all();
        dd($all);
    }
    public function CompanyCategory(){
        $listcomp = CompanyCategory::all();
        return view('backend.CompanyCategory')->with(['listcomp'=>$listcomp]);
    }
    public function CompanyCategory_(Request $request){
        $all = $request->all();

        $addcompanycategory = new CompanyCategory();
        $addcompanycategory->company_category = $request['companycategory'];
        $addcompanycategory->save();

        return back()->with('success','you successfully added a new company category');

    }
    public function EditCompanyCategory(Request $request){
        $id = $request['id'];
        $editcomp = CompanyCategory::where('id',$id)->get();
        return view('backend.EditCompanyCategory')->with(['editcomp'=>$editcomp]);
    }
    public function EditCompanyCategory_(Request $request){
        $id  = $request['id'];
        $updatecomp = CompanyCategory::find($id);
        $updatecomp->company_category = $request['companycategory'];
        $updatecomp->save();

        return redirect()->route('backend.CompanyCategory')->with('success','You have successful updated company category');
    }
    public function DeleteCompanycategory(Request $request){
        $id = $request['id'];
        $deletecomp = CompanyCategory::find($id);
        $deletecomp->delete();
        return back();
    }
    public function AddSubCompanyCat(){
        $listcomp = CompanyCategory::all();
        $listsub = SubCompany::all();
        return view('backend.AddSubCompanyCat')->with(['listcomp'=>$listcomp,'listsub'=>$listsub]);
    }
    public function AddSubCompanyCat_(Request $request){
        $all = $request->all();

        $addsubcomp = new SubCompany();
        $addsubcomp->companycategory = $request['companycategory'];
        $addsubcomp->subcompanycategory = $request['subcompanycategory'];
        $addsubcomp->seatingcapacity = '';
        $addsubcomp->numberofroom = $request['numberofroom'];
        $addsubcomp->Grade = $request['Grade'];
        $addsubcomp->save();

        return back()->with('success','You have successful added company category');

    }
    public function EditSubCompany(Request $request){
        $id = $request['id'];
        $listcomp = CompanyCategory::all();
        $editsub = SubCompany::where('id',$id)->get();

        return view('backend.EditSubCompany')->with(['editsub'=>$editsub,'listcomp'=>$listcomp]);
    }
    public function EditSubCompany_(Request $request){
        $id = $request['id'];
        $editsub = SubCompany::find($id);
        $editsub->companycategory = $request['companycategory'];
        $editsub->subcompanycategory = $request['subcompanycategory'];
        $editsub->seatingcapacity = '';
        $editsub->numberofroom = $request['numberofroom'];
        $editsub->Grade = $request['Grade'];
        $editsub->save();
        return redirect()->route('backend.AddSubCompanyCat')->with('success','You have successful updated Sub category');
    }
    public function DeleteSubCompany(Request $request){
        $id = $request['id'];
        $editsub = SubCompany::find($id);
        $editsub->delete();
        return redirect()->route('backend.AddSubCompanyCat')->with('success','You have successful deleted Sub category');
    }
    public function MemberPhotocategory(){
        $listcat = PhotoCategories::all();
        return view('backend.MemberPhotocategory')->with(['listcat'=>$listcat]);
    }
    public function Photocategoriesupload(Request $request){
        $all = $request->all();

        $image = $request->file('categorypicture');
        $uploadcate = new PhotoCategories();
        $uploadcate->categoryname = $request['categoryname'];
        $uploadcate->categorypicture = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $input['categorypicture'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/photocategories');
        $image->move($destinationPath, $imagename);
        $uploadcate->save();
        return back()->with('success','you have successfully upload a new record');
    }
    public function PhotocategoriesuploadEdit(Request $request){
        $id  = $request['id'];
        $categorypicture = $request['categorypicture'];
        $image = $request->file('categorypicture');

        if(empty($categorypicture)){
            $updatePhotocat = PhotoCategories::find($id);
            $updatePhotocat->categoryname = $request['categoryname'];
            $updatePhotocat->save();
            return back()->with('success','you have successfully upload a new record');
        }else{
            $updatePhotocat = PhotoCategories::find($id);
            $updatePhotocat->categoryname = $request['categoryname'];
            $updatePhotocat->categorypicture = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $input['categorypicture'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/photocategories');
            $image->move($destinationPath, $imagename);
            $updatePhotocat->save();
            return back()->with('success','you have successfully upload a new record');
        }

    }
    public function PhotocategoriesDelete(Request $request){
        $id = $request['id'];
        $deletephotocat = PhotoCategories::find($id);
        $deletephotocat->delete();
        return back()->with('success','you have successfully delete a photo category');
    }

    public function MembersProfile(){
        $user_id = \Auth::user()->id;
        $getjoinmemberid = User::where('id',$user_id)->value('joinmember_id');

        $listmembers = JoinMember::where('JoinMember.id',$getjoinmemberid)
            ->orderBy('created_at', 'desc')
            ->where('Memberstatus','No Status yet')
            ->orWhere('Memberstatus', 'Approved')
            ->join('users', 'users.id', '=', 'JoinMember.user_id')
            ->select('JoinMember.*','users.name')
            ->get();
        return view('backend.MembersProfile')->with(['listmembers'=>$listmembers]);

    }
    public function MemberAccount(){
        $user_id = \Auth::user()->id;
        $listaccount = User::where('id',$user_id)->get();
        return view('backend.MemberAccount')->with(['listaccount'=>$listaccount]);
    }
    public function MemberUploadPhoto(){
        $listcat = PhotoCategories::all();
        $user_id = \Auth::user()->id;
        $listlibrary = MemberPhotoLibrary::where('member_id',$user_id)->get();
        return view('backend.MemberUploadPhoto')->with(['listlibrary'=>$listlibrary,'listcat'=>$listcat]);
    }
    public function MemberUploadPhotoUpload(Request $request){
        $all = $request->all();
        $user_id = \Auth::user()->id;
        $photocategories = $request['photocategories'];
        $image = $request->file('filetoupload');

        foreach ($photocategories as $key => $photocategories) {
            $uploaphoto = new MemberPhotoLibrary();
            $uploaphoto->phototitle = $request['phototitle'];
            $uploaphoto->photodescription = $request['photodescription'];
            $uploaphoto->photocategories = $request['photocategories'][$key];
            $uploaphoto->member_id = $user_id;
            $uploaphoto->photoname = $image->getClientOriginalName();
            $uploaphoto->save();
        }
        $imagename = $image->getClientOriginalName();
        $input['filetoupload'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/MemberPhotoLibrary');
        $image->move($destinationPath, $imagename);
        return back()->with('success','you have successfully uploaded a new photo');

    }
    public function MemberUploadPhotoUploadEdit(Request $request){
        $id = $request['id'];
        $image = $request->file('filetoupload');
        $filetoupload = $request['filetoupload'];
        $user_id = \Auth::user()->id;
        if(empty($filetoupload)){
            $updatelibrary = MemberPhotoLibrary::find($id);
            $updatelibrary->phototitle = $request['phototitle'];
            $updatelibrary->photodescription = $request['photodescription'];
            $updatelibrary->member_id = $user_id;
            $updatelibrary->save();
            return back()->with('success','you have successfully updated a new photo');
        }else{
            $updatelibrary = MemberPhotoLibrary::find($id);
            $updatelibrary->phototitle = $request['phototitle'];
            $updatelibrary->photodescription = $request['photodescription'];
            $updatelibrary->member_id = $user_id;
            $updatelibrary->photoname = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $input['filetoupload'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/MemberPhotoLibrary');
            $image->move($destinationPath, $imagename);
            $updatelibrary->save();
            return back()->with('success','you have successfully updated a new photo');
        }


    }
    public function MemberUploadPhotoUploadDelete(Request $request){
        $id = $request['id'];
        $deletelibrary = MemberPhotoLibrary::find($id);
        $deletelibrary->delete();
        return back()->with('success','you have successfully delete a photo');
    }
    public function AddAttractions(){
        $listconference = Conferences::all();
        $listactivity = ActivitySummary::all();
        $listpackages = Packages::join('conferences', 'conferences.id', '=', 'packages.conference_id')
                ->select('packages.*', 'conferences.conference_title')
                ->get();
        return view('backend.AddAttractions')->with(['listactivity'=>$listactivity,'listconference'=>$listconference,'listpackages'=>$listpackages]);
    }
    public function DomesticPackage(){
        $listconference = Conferences::all();
        $listactivity = ActivitySummary::all();
        $listpackages = DomesticPackages::all();
        return view('backend.DomesticPackage')->with(['listactivity'=>$listactivity,'listconference'=>$listconference,'listpackages'=>$listpackages]);
    }
    public function ListPackages(){
        $listactivity = ActivitySummary::all();
        return view('backend.ListPackages')->with(['listactivity'=>$listactivity]);
    }
    public function UploadPackage(Request $request){
        $all = $request->all();
//        dd($all);
        $addsummary = new ActivitySummary();
        $addsummary->activitytitle = $request['activitytitle'];
        $addsummary->activitysummary = $request['activitysummary'];
        $addsummary->company_name = $request['company_name'];
        $addsummary->company_email = $request['company_email'];
        $addsummary->company_phone = $request['company_phone'];

        $image = $request->file('activityimage');
        $addsummary->activityimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/ActivityCoverImage');
        $image->move($destinationPath, $imagename);
        $addsummary->save();

        $lastid = $addsummary->id;
        $activity_day = $request['activityday'];

        foreach ($activity_day  as $key => $activity_days){
            $adddetails = new ActivityDetails();
            $adddetails->activityday = $request['activityday'][$key];
            $adddetails->activitylocation = $request['activitylocation'][$key];
            $adddetails->activitydetails = $request['activitydetails'][$key];
            $adddetails->activitysummary_id = $lastid;
            $adddetails->save();
        }
        return back()->with('success','you successfully added a new package');

    }
    public function EditActivitySummary(Request $request){
        $all =$request->all();
//        dd($all);
        $id = $request['id'];
        $editsummary = ActivitySummary::find($id);

        if($request->file('activityimage')){

            $editsummary->activitytitle = $request['activitytitle'];
            $editsummary->activitysummary = $request['activity_summary'];
            $editsummary->company_name = $request['company_name'];
            $editsummary->company_email = $request['company_email'];
            $editsummary->company_phone = $request['company_phone'];

            $image = $request->file('activityimage');
            $editsummary->activityimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/ActivityCoverImage');
            $image->move($destinationPath, $imagename);
            $editsummary->save();

            return back()->with('success','you successfully edited destination');
        }else{

            $editsummary->activitytitle = $request['activitytitle'];
            $editsummary->activitysummary = $request['activity_summary'];
            $editsummary->company_name = $request['company_name'];
            $editsummary->company_email = $request['company_email'];
            $editsummary->company_phone = $request['company_phone'];
            $editsummary->save();
            return back()->with('success','you successfully edited destination');
        }
    }
    public function EditActivityDetails(Request $request){
        $id = $request['id'];
        $editdetails = ActivityDetails::find($id);

        $editdetails->activityday = $request['activityday'];
        $editdetails->activitylocation = $request['activitylocation'];
        $editdetails->activitydetails = $request['activitydetails'];
        $editdetails->save();

        return back()->with('success','you successfully edited destination');

    }
    public function DeleteActivitySummary(Request $request){
        $id = $request['id'];
        $deletesummary = ActivitySummary::find($id);
        $deletesummary->delete();
        $deleteactivity = ActivityDetails::where('activitysummary_id',$id)->delete();
        return back()->with('success','you successfully deleted destination');
    }

    /*SHOW CONFERENCE */
    public function Conference(){
        $list = Conferences::all();
        return view('backend.Conference')->with(['list'=>$list]);
    }

    /*ADD CONFERENCE */
    public function AddConference(Request $request){
        $all = $request->all();
        $add_conference = new Conferences();

        $image = $request->file('conference_image_name');
        $new_name = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('/ConferenceImages'), $new_name);
        $add_conference->conference_title = $request['conference_title'];
        $add_conference->conference_image_name = $new_name;
        $add_conference->save();
        return back()->with('success','you have successfully added a conference');
    }

    /*UPDATE CONFERENCE*/
    public function UpdateConference(Request $request){
        $all = $request->all();
        $id = $request['id'];

        if($request->file('conference_image_name')){
            $update = Conferences::find($id);
            $image = $request->file('conference_image_name');
            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('/ConferenceImages'), $new_name);
            $update->conference_title = $request['conference_title'];
            $update->conference_image_name = $new_name;
            $update->save();
        }else{
            $update = Conferences::find($id);
            $update->conference_title = $request['conference_title'];
            $update->save();

        }

        return back()->with('success','you have successfully update conference');
    }
    public function DeleteConference(Request $request){
        $id = $request['id'];
        $delete = Conferences::find($id);
        $delete->delete();
        $deletepackages = Packages::where('conference_id',$id)->delete();
        return back()->with('success','you have successfully deleted conference');
    }

    /*Add PACKAGES*/

    public function AddPackages(Request $request){
        $all = $request->all();
//        dd($all);
        $add = new Packages();
        $image = $request->file('package_image_name');
        $new_name = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('/PackageImages'), $new_name);
        $add->package_title = $request['package_title'];
        $add->conference_id = $request['conference_id'];
        $add->package_details = $request['package_details'];
        $add->package_image_name = $new_name;
        $add->company_name = $request['company_name'];
        $add->company_email = $request['company_email'];
        $add->company_phone = $request['company_phone'];
        $add->save();

        return back()->with('success','you have successfully added conference');
    }
    public function UpdatePackages(Request $request){
        $all = $request->all();
        $id = $request['id'];
//        dd($id);
        if($request->file('package_image_name')){
            $update = Packages::find($id);
            $image = $request->file('package_image_name');
            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('/PackageImages'), $new_name);
            $update->package_title = $request['package_title'];
            $update->conference_id = $request['conference_id'];
            $update->package_details = $request['package_details'];
            $update->company_name = $request['company_name'];
            $update->company_email = $request['company_email'];
            $update->company_phone = $request['company_phone'];
            $update->package_image_name = $new_name;
            $update->save();
        }else{
            $update = Packages::find($id);
            $update->package_title = $request['package_title'];
            $update->conference_id = $request['conference_id'];
            $update->package_details = $request['package_details'];
            $update->company_name = $request['company_name'];
            $update->company_email = $request['company_email'];
            $update->company_phone = $request['company_phone'];
            $update->save();
        }
        return back()->with('success','you have successfully updated conference');
    }
    public function DeletePackages(Request $request){
        $id = $request['id'];
        $delete = Packages::find($id);
        $delete->delete();
        return back()->with('success','you have successfully deleted package');
    }

    public function UploadDomesticPackage(Request $request){
        $all = $request->all();
//        dd($all);
        $add = new DomesticPackages();
        $image = $request->file('package_image_name');
        $docum = $request->file('package_document');

        $new_name = rand() . '.' . $image->getClientOriginalExtension();
        $new_name_document = rand() . '.' . $docum->getClientOriginalExtension();

        $image->move(public_path('/PackageImages'), $new_name);
        $docum->move(public_path('/PackageImages'), $new_name_document);

        $add->package_title = $request['package_title'];
        $add->package_category = $request['package_category'];
        $add->package_price = $request['package_price'];
        $add->package_description = $request['package_description'];
        $add->package_image_name = $new_name;
        $add->package_document = $new_name_document;

        $add->company_name = $request['company_name'];
        $add->company_email = $request['company_email'];
        $add->company_phone = $request['company_phone'];
        $add->save();

        return back()->with('success','you have successfully added domestic packages');
    }
    public function EditDomesticPackage(Request $request){
        $all =$request->all();
//        dd($all);
        $id = $request['id'];
        $editsummary = DomesticPackages::find($id);

        if($request->file('package_image_name') and $request->file('package_document')){
            $image = $request->file('package_image_name');
            $docum = $request->file('package_document');

            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $new_name_document = rand() . '.' . $docum->getClientOriginalExtension();

            $image->move(public_path('/PackageImages'), $new_name);
            $docum->move(public_path('/PackageImages'), $new_name_document);

            $editsummary->package_title = $request['package_title'];
            $editsummary->package_category = $request['package_category'];
            $editsummary->package_price = $request['package_price'];
            $editsummary->package_description = $request['package_description'];

            $editsummary->package_image_name = $new_name;
            $editsummary->package_document = $new_name_document;

            $editsummary->company_name = $request['company_name'];
            $editsummary->company_email = $request['company_email'];
            $editsummary->company_phone = $request['company_phone'];
            $editsummary->save();

            return back()->with('success','you have successfully updated domestic packages');

        }else if($request->file('package_image_name')){

            $image = $request->file('package_image_name');

            $new_name = rand() . '.' . $image->getClientOriginalExtension();

            $image->move(public_path('/PackageImages'), $new_name);

            $editsummary->package_title = $request['package_title'];
            $editsummary->package_category = $request['package_category'];
            $editsummary->package_price = $request['package_price'];
            $editsummary->package_description = $request['package_description'];
            $editsummary->package_image_name = $new_name;

            $editsummary->company_name = $request['company_name'];
            $editsummary->company_email = $request['company_email'];
            $editsummary->company_phone = $request['company_phone'];
            $editsummary->save();

            return back()->with('success','you have successfully updated domestic packages');

        }else if($request->file('package_document')){
            $docum = $request->file('package_document');

            $new_name_document = rand() . '.' . $docum->getClientOriginalExtension();

            $docum->move(public_path('/PackageImages'), $new_name_document);

            $editsummary->package_title = $request['package_title'];
            $editsummary->package_category = $request['package_category'];
            $editsummary->package_price = $request['package_price'];
            $editsummary->package_description = $request['package_description'];
            $editsummary->package_document = $new_name_document;

            $editsummary->company_name = $request['company_name'];
            $editsummary->company_email = $request['company_email'];
            $editsummary->company_phone = $request['company_phone'];
            $editsummary->save();

            return back()->with('success','you have successfully updated domestic packages');
        }
        else{

            $editsummary->package_title = $request['package_title'];
            $editsummary->package_category = $request['package_category'];
            $editsummary->package_price = $request['package_price'];
            $editsummary->package_description = $request['package_description'];
            $editsummary->company_name = $request['company_name'];
            $editsummary->company_email = $request['company_email'];
            $editsummary->company_phone = $request['company_phone'];
            $editsummary->save();

            return back()->with('success','you have successfully updated domestic packages');
        }
    }
    public function DeleteDomesticPackage(Request $request){
        $id = $request['id'];
        $delete = DomesticPackages::find($id);
        $delete->delete();
        return back()->with('success','you have successfully deleted domestic packages');
    }
}
