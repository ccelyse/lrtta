<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberPhotoLibraryCategories extends Model
{
    protected $table = "memberphotolibrarycategories";
    protected $fillable = ['id','memberphotolibrary_id','photocategories'];
}
