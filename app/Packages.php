<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packages extends Model
{
    protected $table = "packages";
    protected $fillable = ['id','package_title','package_image_name','conference_id','package_details'
        ,'company_name','company_email','company_phone'];
}
