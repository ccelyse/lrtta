<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivitySummary extends Model
{
    protected $table = "ActivitySummary";
    protected $fillable = ['id','activitytitle','activitysummary','activityimage','company_name','company_email','company_phone'];
}
