<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomesticPackages extends Model
{
    protected $table = "domestic_packages";
    protected $fillable = ['id','package_title','package_image_name','package_document'
        ,'company_name','company_email','company_phone','package_category','package_price','package_description'];
}
