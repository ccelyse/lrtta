@extends('layouts.master')

@section('title', 'RTTA')

@section('content')
    <style>
        .spb-asset-content p {
            color: #000 !important;
        }
        .title-wrap .spb-heading>span {
            display: inline-block;
            text-transform: uppercase;
        }
        .team-member .team-member-name {
            margin-bottom: 0;
            font-size: 11px;
        }
        .team-member-position{
            line-height: 20px !important;
        }
    </style>
    @include('layouts.topmenu')

    <div id="sf-mobile-slideout-backdrop"></div>
    <div id="main-container" class="clearfix">
        <div class="fancy-heading-wrap  fancy-style">
            <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/19180566172_b4f367cf8b_k.jpg');" data-height="475" data-img-width="2000" data-img-height="800">
                <span class="media-overlay" style="background-color:#3c3b3b;opacity:0;"></span>
                <div class="heading-text container" data-textalign="left">
                    <h1 class="entry-title">General Tips</h1>
                </div>
            </div>
        </div>
        <div class="inner-container-wrap">
            <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
                <div class="clearfix">
                    <div class="page-content hfeed clearfix">
                        <div class="clearfix post-13116 page type-page status-publish hentry" id="13116">

                            <section class="container ">
                                <div class="row">
                                    <div class="blank_spacer col-sm-12  " style="height:60px;"></div>
                                </div>
                            </section>

                            <section class="container ">
                                <div class="row">
                                    <div class="spb_content_element col-sm-6 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Common Kinyarwanda Words</span></h3>
                                            </div>
                                            <p>Mwaramutse – Good morning</p>
                                            <p>Mwirirwe – Good afternoon/evening</p>
                                            <p>Muramuke – Good night</p>
                                            <p>Amakuru yawe – How are you?</p>
                                            <p>Ni Meza – Fine</p>
                                            <p>Murakoze – Thank you</p>
                                            <p>Cyane – Very much</p>
                                            <p>Yego – Yes</p>
                                            <p>Oya – No</p>
                                            <p>Amafaranga – Money</p>
                                        </div>
                                    </div>

                                    <div class="spb_content_element col-sm-6 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Official Holidays</span></h3>
                                            </div>
                                            <p><strong>Official holidays in public and private sector shall be determined as follows:</strong></p>
                                            <p>1st January: New Year’s Day</p>
                                            <p>2nd January: Day after New Year’s Day</p>
                                            <p>1st February: National Heroes Day</p>
                                            <p>Good Friday</p>
                                            <p>Easter Monday </p>
                                            <p>7th April: Genocide against the Tutsi Memorial Day</p>
                                            <p>1st May: Labor Day</p>
                                            <p>1st July: Independence Day</p>
                                            <p>4th July: Liberation Day</p>
                                            <p>Friday of first week of August: Umuganura Day</p>
                                            <p>15th August: Assumption Day</p>
                                            <p>25th December: Christmas Day</p>
                                            <p>26th December: Boxing Day</p>
                                            <p>EID EL FITR: the date shall be announced each year by Rwanda Moslems Association</p>
                                            <p>EID AL-ADHA: the date shall be announced each year by Rwanda Moslems Association</p>
                                            <p><strong>Coincidence of official holidays with the days of weekend</strong></p>
                                            <p>Except 7th April, Genocide against the Tutsi Memorial Day, if an official holiday falls on one (1) day of the weekend, the following working day shall be an official holiday. If two (2) consecutive official holidays fall on a day of weekend, the two (2) official holidays shall be compensated in one (1) working day that follows. In case of coincidence of two (2) official holidays, the following working day shall be an official holiday to compensate one (1) of the two (2) coinciding official holidays.</p>
                                        </div>
                                    </div>
                                    <div class="spb_content_element col-sm-12 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                           <img src="images/IMG_8466a-1024x307a.jpg" style="width: 100%">
                                        </div>
                                    </div>
                                    <div class="spb_content_element col-sm-6 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>When to Visit</span></h3>
                                            </div>
                                            <p>Rwanda can be visited throughout the year. Gorilla trekking and other forest walks are less demanding during the drier months. The European winter is the best time for birds, as Pala arctic migrants supplement resident species.</p>
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>What to bring</span></h3>
                                            </div>
                                            <p>Binoculars greatly enhance game drives, forest walks and bird watching. Bring a camera and an adequate stock of film. Print film is available but transparency film is not. Toiletries and other essentials can be bought in the cities.</p>
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>What to Wear</span></h3>
                                            </div>
                                            <p>Dress codes are informal. Daytime temperatures are generally warm, so bring lots of light clothing, supplemented by light sweaters for the cool evenings and heavier clothing for the Volcanic and Nyungwe National parks. When tracking gorillas, wear sturdier clothing to protect against stinging nettles, and solid walking shoes. A hat and sunglasses provide protection against the sun, and a waterproof jacket may come in handy in the moist mountains.</p>
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Communications</span></h3>
                                            </div>
                                            <p>Rwanda has an excellent cell phone network covering almost the entire country. International phone calls can be made easily. Appropriate SIM cards for the network are readily available everywhere, even in remote towns. Cell phones can be purchased or rented from major shops in Kigali. Most towns have several Internet cafes and computer centres.</p>
                                        </div>
                                    </div>
                                    <div class="spb_content_element col-sm-6 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Environment</span></h3>
                                            </div>
                                            <p>Rwanda has strict environmental laws. The use of plastic bags (polythene papers) has been banned. Beware not to carry such since they will not be allowed into the country at the border checkpoints. Kigali is a clean and green city. You are expected to throw your litter in rubbish cans which are fitted all over. Walk along the provided foot paths. Do not step on the grass.</p>
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Safety and Health</span></h3>
                                            </div>
                                            <p>A certificate of yellow-fever vaccination is required. Much of Rwanda lies at too high an elevation for malaria to be a major concern, but the disease is present and prophylactic drugs are strongly recommended. It is advisable not to drink tap water. Bottled mineral water can be bought in all towns. Hospitals are located in all major towns.</p>
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Safety and Health</span></h3>
                                            </div>
                                            <p>A certificate of yellow-fever vaccination is required. Much of Rwanda lies at too high an elevation for malaria to be a major concern, but the disease is present and prophylactic drugs are strongly recommended. It is advisable not to drink tap water. Bottled mineral water can be bought in all towns. Hospitals are located in all major towns.</p>

                                        </div>
                                    </div>

                                </div>
                            </section>


                            <div class="link-pages"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sf-full-header-search-backdrop"></div>
    </div>
@include('layouts.footer')
@endsection