@extends('layouts.master')

@section('title', 'RTTA')

@section('content')

    @include('layouts.topmenu')
<style>
    .sf-button.accent {
        color: #fff;
        background-color: #6b442b;
        border-color: #6b442b;
    }
    .title-wrap {
         margin-bottom: 0px !important;
        height: auto;
        overflow: hidden;
    }
    .title-wrap .spb-heading>span {
        display: inline-block;
        text-transform: uppercase;
        font-family: "Open Sans";
        line-height: 28px;
        font-weight: 700;
        font-style: normal;
        font-size: 18px;
    }
    .spb-asset-content p {
        color: #000 !important;
    }
    p,span,b{
        font-family: "Open Sans" !important;
        line-height: 28px !important;
        font-weight: 400 !important;
        font-style: normal !important;
        font-size: 15px !important;
        color:#000 !important;
        background: transparent !important;
    }
    h2{
        background: transparent !important;
    }
    h3, .single_variation_wrap .single_variation span.price, .sf-promo-bar p.standard, .sf-promo-bar.text-size-standard p, .sf-icon-box-animated-alt .front .back-title {
        font-family: "Open Sans";
        line-height: 28px;
        font-weight: 700;
        font-style: normal;
        font-size: 18px;
        text-transform: uppercase;
        background: transparent !important;
    }
    a, .ui-widget-content a {
        color: #414141;
    }
    .spb_tabs .nav-tabs li.active a span:after {
        background-color: #00634c;
    }
    .nav-tabs {
        border-bottom: 1px solid #01644c !important;
    }
    .spb_tabs .nav-tabs li:hover a, .spb_tour .nav-tabs li:hover a, .spb_tabs .nav-tabs li.active a, .spb_tour .nav-tabs li.active a {
        background: #fff;
        border-color: #03644c !important;
        color: #222 !important;
    }
    .spb_tabs .nav-tabs li a, .spb_tour .nav-tabs li a {
        background-color: #f7f7f7;
        border-color: #02634b !important;
    }
    .spb_tabs .nav-tabs li:first-child a {
        border-left-width: 1px !important;
        text-decoration: none;
    }
    li.li2 {
        font-family: "Open Sans" !important;
        line-height: 28px !important;
        font-weight: 400 !important;
        font-style: normal !important;
        font-size: 15px !important;
        color: #000 !important;
    }
    /*img{*/
        /*width: 100% !important;*/
    /*}*/
</style>
    <div id="sf-mobile-slideout-backdrop"></div>
    @foreach($summary as $data)
    <div id="main-container" class="clearfix">
    <div class="fancy-heading-wrap  fancy-style">
        <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('ActivityCoverImage/{{$data->activityimage}}');" data-height="475" data-img-width="2000" data-img-height="800">
            <span class="media-overlay" style="background-color:transparent;opacity:0;"></span>
            <div class="heading-text container" data-textalign="left">
                <h1 class="entry-title">{{$data->activitytitle}}</h1>
            </div>
        </div>
    </div>



        <div class="inner-container-wrap">
            <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
                <div class="clearfix">
                    <div class="page-content hfeed clearfix">
                        <div class="clearfix post-13116 page type-page status-publish hentry" id="13116">
                            <section class="container ">
                                <div class="row">
                                    <div class="blank_spacer col-sm-12  " style="height:60px;"></div>
                                </div>
                            </section>
                            <section class="row ">
                                <div class="spb_tabs tabs-type-standard spb_content_element col-sm-12" data-interval="0">
                                    <div class="spb-asset-content spb_wrapper spb_tabs_wrapper">
                                        <ul class="nav nav-tabs center-tabs"><li class="active"><a href="index.html#tabone-2" data-toggle="tab"><span>PACKAGE INFORMATION</span></a></li><li><a href="index.html#tabtwo-2" data-toggle="tab"><span>INQUIRE</span></a></li></ul>
                                        <div class="tab-content">
                                            <div id="tabone-2" class="tab-pane load">
                                                <section class="container ">
                                                    <div class="row">
                                                        <div class="spb_content_element col-sm-8 col-md-offset-2 spb_text_column">
                                                            <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">

                                                                <?php
                                                                $summary = $data->activitysummary;
                                                                ?>
                                                                <div><?php echo $summary;?></div>
                                                            </div>

                                                        </div>
                                                        @foreach($details as $detail)
                                                            <div class="spb_content_element col-sm-8 col-md-offset-2 spb_text_column">
                                                                <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                                    <div class="title-wrap">
                                                                        <h3 class="spb-text-heading">{{$detail->activitylocation}}</h3>
                                                                    </div>
                                                                    <div class="title-wrap">
                                                                        <h3 class="spb-text-heading">{{$detail->activityday}}</h3>
                                                                    </div>
                                                                    <?php
                                                                    $summaryd = $detail->activitydetails;
                                                                    ?>
                                                                    <div><?php echo $summaryd;?></div>
                                                                </div>

                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </section>
                                            </div>
                                            <div id="tabtwo-2" class="tab-pane load">
                                                <section class="container ">
                                                    <div class="row">
                                                        <div class="spb_content_element col-sm-8 col-md-offset-2 spb_text_column">
                                                            <div class="title-wrap">
                                                                <h3 class="spb-text-heading text-center">Contact</h3>
                                                            </div>

                                                            <p class="text"><strong><i class="fab fa-whatsapp"></i></strong> +25{{$data->company_phone}}</p>
                                                            <p class="text"><strong>Company Name:</strong> {{$data->company_name}}</p>
                                                            {{--<p class="text"><strong> Email:</strong> <a href="" class="__cf_email__" style="color: #000 !important;">{{$data->company_email}}</a></p>--}}

                                                            <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                                <div role="form" class="wpcf7" id="wpcf7-f15387-p13072-o1" lang="en-US" dir="ltr">
                                                                    <div class="screen-reader-response"></div>
                                                                    <form action="{{url('BookNow')}}" method="post" class="wpcf7-form">
                                                                        {{ csrf_field() }}
                                                                        <p><span class="wpcf7-form-control-wrap name"><input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Name" required/></span><br />
                                                                        <p><span class="wpcf7-form-control-wrap name"><input type="text" name="company_name" value="{{$data->company_name}}" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Name" required hidden/></span><br />
                                                                        <p><span class="wpcf7-form-control-wrap email"><input type="email" name="fromemail" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Email" /></span><br />
                                                                        <p><span class="wpcf7-form-control-wrap email"><input type="email" name="toemail" value="{{$data->company_email}}" size="40" class="wpcf7-form-control wpcf7-text" hidden/></span><br />
                                                                            <span class="wpcf7-form-control-wrap subject"><input type="text" name="subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Subject" required/></span><br />
                                                                            <span class="wpcf7-form-control-wrap message"><textarea name="message" cols="40" rows="5" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Message" required></textarea></span><br />
                                                                            <input type="submit" value="Book Now" class="wpcf7-form-control wpcf7-submit" />
                                                                        </p>
                                                                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                            {{--<div id="tabthree-2" class="tab-pane load">--}}
                                                {{--<section class="container ">--}}
                                                    {{--<div class="row">--}}

                                                            {{----}}
                                                    {{--</div>--}}
                                                {{--</section>--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <div class="link-pages"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sf-full-header-search-backdrop"></div>
</div>
@endforeach
    @include('layouts.footer')
@endsection