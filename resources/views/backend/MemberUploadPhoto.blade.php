@extends('backend.layout.master')

@section('title', 'RSGA')

@section('content')
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary:hover{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
    </style>
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{url('MemberUploadPhotoUpload')}}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Photo</label>
                                                            <input type="file" id="projectinput1" class="form-control"
                                                                   name="filetoupload" required>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <label for="projectinput1">Photo Categories</label>
                                                            <select class="select2 form-control" multiple="multiple" name="photocategories[]">
                                                                @foreach($listcat as $data)
                                                                    <option value="{{$data->categoryname}}">{{$data->categoryname}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Photo Title</label>
                                                            <input type="text" id="projectinput1" class="form-control"
                                                                   name="phototitle" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Photo Description</label>
                                                            <textarea class='form-control' name="photodescription" rows="5" required></textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <section id="complex-header">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Photo Library</h4>

                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Photo Title</th>
                                                <th>Photo</th>
                                                <th>Photo Category</th>
                                                <th>Photo Description</th>
                                                <th>Date Created</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listlibrary as $data)
                                            <tr>
                                            <td>{{$data->phototitle}}</td>
                                            <td>
                                            <img src="MemberPhotoLibrary/{{$data->photoname}}" style="width: 100px;">
                                            </td>
                                            <td>{{$data->photocategories}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                            data-toggle="modal"
                                                            data-target="#editcat{{$data->id}}">
                                                        View Description
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="editcat{{$data->id}}" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1">Photo Description</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="row  multi-field">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="projectinput1">Photo Description</label>
                                                                                <textarea class='form-control' name="photodescription" rows="5" required>{{$data->photodescription}}</textarea>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            <td>{{$data->created_at}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                            data-toggle="modal"
                                                            data-target="#editlibrary{{$data->id}}">
                                                        Edit
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade text-left" id="editlibrary{{$data->id}}" tabindex="-1"
                                                         role="dialog" aria-labelledby="myModalLabel1"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel1"> Edit Photo Library</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form class="form-horizontal form-simple" method="POST"
                                                                          action="{{ url('MemberUploadPhotoUploadEdit') }}"
                                                                          enctype="multipart/form-data">
                                                                        {{ csrf_field() }}

                                                                        <div class="row  multi-field">

                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Photo</label>
                                                                                        <input type="file" id="projectinput1" class="form-control"
                                                                                               name="filetoupload">
                                                                                        <input type="text" id="projectinput1" class="form-control"
                                                                                               name="id" value="{{$data->id}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Photo Categories</label>
                                                                                        <select class="form-control" id="basic" name="photocategories" disabled>
                                                                                            <option value="{{$data->photocategories}}">{{$data->photocategories}}</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Photo Title</label>
                                                                                        <input type="text" id="projectinput1" class="form-control"
                                                                                               name="phototitle" value="{{$data->phototitle}}" required>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Photo Description</label>
                                                                                        <textarea class='form-control' name="photodescription" rows="5" required>{{$data->photodescription}}</textarea>
                                                                                    </div>
                                                                                </div>

                                                                            <div class="col-md-12" style="margin-bottom: 15px">
                                                                                <img src="MemberPhotoLibrary/{{$data->photoname}}" style="width: 150px;">
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <button type="submit" class="btn btn-login">
                                                                                    <i class="la la-check-square-o"></i> Send
                                                                                </button>
                                                                            </div>

                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            <td><a href="{{ route('backend.MemberUploadPhotoUploadDelete',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                            </tr>

                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>

                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>
                </div>
            </div>
        </div>
    </div>

@endsection
