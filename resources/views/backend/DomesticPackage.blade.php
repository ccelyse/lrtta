@extends('backend.layout.master')

@section('title', 'RTTA')

@section('content')
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #006837 !important;
            border-color:#006837 !important;
        }
        .btn-primary:hover{
            background-color: #006837 !important;
            border-color:#006837 !important;
        }
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-dark1 {
            color: #000 !important;
            background-color: transparent;
            border-color: #b4753c !important;
        }
        .btn-dark1:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: #b4753c !important;
        }
        .MsoNormal,span p{
            font-family: Quicksand,Georgia,'Times New Roman',Times,serif !important;
            /*display: inline;*/
            /*color: #000 !important;*/
        }
        span{
            font-family: Quicksand,Georgia,'Times New Roman',Times,serif !important;
        }
        span > p.MsoNormal{
            display: unset !important;
        }
    </style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('.multi-field-wrapper').each(function() {
                var $wrapper = $('.multi-fields', this);
                $(".add-field", $(this)).click(function(e) {
                    $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                });
                $('.multi-field .remove-field', $wrapper).click(function() {
                    if ($('.multi-field', $wrapper).length > 1)
                        $(this).parent('.multi-field').remove();
                });
            });
        });
        $(document).ready(function()
        {
            $('.multi-field-wrapper2').each(function() {
                var $wrapper = $('.multi-fields2', this);
                $(".add-field2", $(this)).click(function(e) {
                    $('.multi-field2:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                });
                $('.multi-field2 .remove-field2', $wrapper).click(function() {
                    if ($('.multi-field2', $wrapper).length > 1)
                        $(this).parent('.multi-field2').remove();
                });
            });
        });
    </script>
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/pages/timeline.min.css">
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <h4 class="card-title" id="basic-layout-form">Add Package</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('UploadDomesticPackage') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Package title</label>
                                                        <input type="text" id="projectinput1" class="form-control" value="{{ old('package_title') }}" placeholder="Package Title"
                                                               name="package_title" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Package cover image</label>
                                                        <input type="file" id="projectinput1" class="form-control"
                                                               name="package_image_name" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {{--<label for="projectinput1">Package cover image</label>--}}
                                                        <label for="projectinput1">Package Document</label>
                                                        <input type="file" id="package_document" class="form-control"
                                                               name="package_document" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {{--<label for="projectinput1">Package cover image</label>--}}
                                                        <label for="projectinput1">Package Category</label>
                                                        <select class="form-control" name="package_category">
                                                            <option value="Western">Western</option>
                                                            <option value="Southwestern">Southwestern</option>
                                                            <option value="Northern">Northern</option>
                                                            <option value="Kigali City">Kigali City</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {{--<label for="projectinput1">Package cover image</label>--}}
                                                        <label for="projectinput1">Package price</label>
                                                        <input type="text" id="package_price" class="form-control"
                                                               name="package_price">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {{--<label for="projectinput1">Package cover image</label>--}}
                                                        <label for="projectinput1">Package Description</label>
                                                        <textarea class="form-control" name="package_description"></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            {{--<section id="timeline" class="timeline-center timeline-wrapper" style="padding-bottom: 10px">--}}
                                                {{--<h3 class="page-title text-center">Activity in details</h3>--}}
                                                {{--<ul class="timeline">--}}
                                                    {{--<li class="timeline-line"></li>--}}
                                                {{--</ul>--}}
                                                {{--<div class="multi-field-wrapper">--}}
                                                    {{--<div class="multi-fields">--}}
                                                        {{--<div class="multi-field">--}}
                                                            {{--<ul class="timeline" style="margin-top: 10px;">--}}
                                                                {{--<li class="timeline-line"></li>--}}
                                                                {{--<li class="timeline-item">--}}
                                                                    {{--<div class="timeline-badge">--}}
                                                                        {{--<span class="bg-red bg-lighten-1" data-toggle="tooltip" data-placement="right" title="Activity in details"><i class="la la-plane"></i></span>--}}
                                                                    {{--</div>--}}
                                                                    {{--<div class="timeline-card card border-grey border-lighten-2">--}}
                                                                        {{--<div class="card-header">--}}
                                                                            {{--<h4 class="card-title"><a href="#">Day</a></h4>--}}
                                                                        {{--</div>--}}
                                                                        {{--<div class="card-content">--}}
                                                                            {{--<div class="row">--}}
                                                                                {{--<div class="col-md-12">--}}
                                                                                    {{--<div class="form-group">--}}
                                                                                        {{--<input type="text" id="projectinput1" class="form-control" value="{{ old('activityday') }}" placeholder="Day 1"--}}
                                                                                               {{--name="activityday[]" >--}}
                                                                                    {{--</div>--}}
                                                                                {{--</div>--}}
                                                                                {{--<div class="col-md-12">--}}
                                                                                    {{--<div class="form-group">--}}
                                                                                        {{--<label for="projectinput1">Activity Location</label>--}}
                                                                                        {{--<input type="text" id="projectinput1" class="form-control" value="{{ old('activitylocation') }}" placeholder="Activity Location"--}}
                                                                                               {{--name="activitylocation[]" >--}}
                                                                                    {{--</div>--}}
                                                                                {{--</div>--}}
                                                                            {{--</div>--}}

                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                {{--</li>--}}
                                                                {{--<li class="timeline-item mt-3">--}}

                                                                    {{--<div class="timeline-card card border-grey border-lighten-2">--}}
                                                                        {{--<div class="card-header">--}}
                                                                            {{--<h4 class="card-title"><a href="#">Activity in detail</a></h4>--}}
                                                                        {{--</div>--}}
                                                                        {{--<div class="col-md-12">--}}
                                                                            {{--<div class="form-group">--}}
                                                                                {{--<label for="projectinput1">Activity in detail</label>--}}
                                                                                {{--<textarea  name="activitydetails[]" class="summernote" id="activity_details" rows="10" ></textarea>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                {{--</li>--}}

                                                            {{--</ul>--}}
                                                            {{--<button type="button" class="remove-field btn btn-dark1" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i>Remove a days</button>--}}
                                                            {{--<button type="button" class="add-field btn btn-dark1" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i>Add more days</button>--}}

                                                        {{--</div>--}}

                                                    {{--</div>--}}

                                                {{--</div>--}}
                                            {{--</section>--}}

                                            <h3 class="page-title">Company Information</h3>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Company Name</label>
                                                        <input type="text" id="projectinput1" class="form-control" value="{{ old('company_name') }}"
                                                               name="company_name">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Company Email</label>
                                                        <input type="text" id="projectinput1" class="form-control" value="{{ old('company_email') }}"
                                                               name="company_email">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Company Phone Number</label>
                                                        <input type="text" id="projectinput1" class="form-control" value="{{ old('company_phone') }}"
                                                               name="company_phone">
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section id="setting">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Package list</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Package Title</th>
                                                    <th>Package Category</th>
                                                    <th>Package Document</th>
                                                    <th>Activity Cover Picture</th>
                                                    <th>Date created</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listpackages as $data)
                                                    <tr>
                                                        <td>{{$data->package_title}}</td>
                                                        <td>{{$data->package_category}}</td>
                                                        <td><a href="PackageImages/{{$data->package_document}}" class="btn btn-icon btn-outline-primary" target="_blank">Package Document</a></td>
                                                        <td><img src="PackageImages/{{$data->package_image_name}}" style="width: 100%;padding-bottom: 10px"></td>
                                                        <td>{{$data->created_at}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary"
                                                                    data-toggle="modal"
                                                                    data-target="#editsummary{{$data->id}}">
                                                                Edit
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="editsummary{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1"> Edit Package</h4>
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('EditDomesticPackage') }}" enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                                <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Package title</label>
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->package_title}}"
                                                                                                   name="package_title" required>
                                                                                            <input type="text" id="id" class="form-control"
                                                                                                   name="id" value="{{$data->id}}" hidden>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Package cover image</label>
                                                                                            <input type="file" id="projectinput1" class="form-control"
                                                                                                   name="package_image_name" value="{{$data->package_title}}">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            {{--<label for="projectinput1">Package cover image</label>--}}
                                                                                            <label for="projectinput1">Package Document</label>
                                                                                            <input type="file" id="package_document" class="form-control"
                                                                                                   name="package_document">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            {{--<label for="projectinput1">Package cover image</label>--}}
                                                                                            <label for="projectinput1">Package Category</label>
                                                                                            <select class="form-control" name="package_category">
                                                                                                <option value="{{$data->package_category}}" selected>{{$data->package_category}}</option>
                                                                                                <option value="Western">Western</option>
                                                                                                <option value="Southwestern">Southwestern</option>
                                                                                                <option value="Northern">Northern</option>
                                                                                                <option value="Eastern">Eastern</option>
                                                                                                <option value="Kigali City">Kigali City</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            {{--<label for="projectinput1">Package cover image</label>--}}
                                                                                            <label for="projectinput1">Package price</label>
                                                                                            <textarea class="form-control" name="package_price">{{$data->package_price}}</textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            {{--<label for="projectinput1">Package cover image</label>--}}
                                                                                            <label for="projectinput1">Package Description</label>
                                                                                            <textarea class="form-control" name="package_description">{{$data->package_description}}</textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <h3 class="page-title">Company Information</h3>
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Company Name</label>
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->company_name}}"
                                                                                                   name="company_name">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Company Email</label>
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->company_email}}"
                                                                                                   name="company_email">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Company Phone Number</label>
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->company_phone}}"
                                                                                                   name="company_phone" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <img src="PackageImages/{{$data->package_image_name}}" style="width: 100%;padding-bottom: 10px">
                                                                                    </div>
{{--                                                                                </div>--}}
                                                                                    <div class="col-md-12">
                                                                                        <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Update</button>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><a href="{{ route('DeleteDomesticPackage',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="summernote-edit-save" hidden>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Summernote Click to edit</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body">

                                                <div class="form-group">
                                                    <button id="edit" class="btn btn-primary" type="button"><i class="la la-pencil"></i> Edit</button>
                                                    <button id="save" class="btn btn-success" type="button"><i class="la la-save"></i> Save</button>
                                                </div>
                                                <div class="form-group">
                                                    <div class="summernote-edit">
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/pages/timeline.min.js"></script>
                    <script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>
                </div>
            </div>
        </div>
    </div>

@endsection
