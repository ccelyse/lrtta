@extends('backend.layout.master')

@section('title', 'RTTA')

@section('content')
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary:hover{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        .btn-dark1 {
            color: #000 !important;
            background-color: transparent;
            border-color: #b4753c !important;
        }
        .btn-dark1:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: #b4753c !important;
        }
    </style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('.multi-field-wrapper').each(function() {
                var $wrapper = $('.multi-fields', this);
                $(".add-field", $(this)).click(function(e) {
                    $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                });
                $('.multi-field .remove-field', $wrapper).click(function() {
                    if ($('.multi-field', $wrapper).length > 1)
                        $(this).parent('.multi-field').remove();
                });
            });
        });
        $(document).ready(function()
        {
            $('.multi-field-wrapper2').each(function() {
                var $wrapper = $('.multi-fields2', this);
                $(".add-field2", $(this)).click(function(e) {
                    $('.multi-field2:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                });
                $('.multi-field2 .remove-field2', $wrapper).click(function() {
                    if ($('.multi-field2', $wrapper).length > 1)
                        $(this).parent('.multi-field2').remove();
                });
            });
        });
    </script>
    <link rel="stylesheet" type="text/css" href="backend/app-assets/css/pages/timeline.min.css">
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->

                    <section id="setting">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Activity list</h4>
                                        @if (session('success'))
                                            <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                                {{ session('success') }}
                                            </div>
                                        @endif
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Activity Title</th>
                                                    <th>Activity Summary</th>
                                                    <th>Activity Cover Picture</th>
                                                    <th>Activity Details</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listactivity as $data)
                                                    <tr>
                                                        <td>{{$data->activitytitle}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary"
                                                                    data-toggle="modal"
                                                                    data-target="#summary{{$data->id}}">
                                                                Activity Summary
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="summary{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1"> Activity Summary</h4>
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <?php
                                                                            $text = $data->activitysummary;
                                                                            echo "<div>$text</div>";
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><img src="ActivityCoverImage/{{$data->activityimage}}" style="width: 100%;padding-bottom: 10px"></td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary"
                                                                    data-toggle="modal"
                                                                    data-target="#details{{$data->id}}">
                                                                Activity Details
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="details{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1"> Activity Summary</h4>
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('EditActivityDetails') }}" enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                            <?php
                                                                            $id = $data->id;
                                                                            $listdetails = \App\ActivityDetails::where('activitysummary_id',$id)->get();
                                                                            ?>

                                                                            @foreach($listdetails as $details)
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$details->activityday}}"
                                                                                                   name="activityday" >
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$details->id}}"
                                                                                                   name="id" >
                                                                                        </div>

                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Activity Location</label>
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$details->activitylocation}}"
                                                                                                   name="activitylocation" >
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Activity in detail</label>

                                                                                            <textarea  name="activitydetails" class="summernote" id="activity_details" rows="10">

                                                                                               <?php
                                                                                                $text = $details->activitydetails;
                                                                                                echo "<div>$text</div>";
                                                                                                ?>
                                                                                            </textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <button type="submit" class="btn btn-primary" style="margin-bottom: 15px"> <i class="la la-check-square-o"></i> Update</button>
                                                                            @endforeach

                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary"
                                                                    data-toggle="modal"
                                                                    data-target="#editsummary{{$data->id}}">
                                                                Edit
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="editsummary{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1"> Activity Summary</h4>
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('EditActivitySummary') }}" enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Activity Title</label>
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->activitytitle}}" placeholder="Activity Title"
                                                                                                   name="activitytitle">

                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->id}}"
                                                                                                   name="id" hidden>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Activity Cover Image</label>
                                                                                            <input type="file" id="projectinput1" class="form-control"
                                                                                                   name="activityimage">
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Activity Summary</label>
                                                                                            <textarea  name="activity_summary" class="summernote" id="activitysummary" rows="10">

                                                                                                <?php
                                                                                                $texts = $data->activitysummary;
                                                                                                echo "<div>$texts</div>";
                                                                                                ?>
                                                                                            </textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                    <h3 class="page-title">Company Information</h3>

                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Company Name</label>
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->company_name}}"
                                                                                                   name="company_name" required>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Company Email</label>
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->company_email}}"
                                                                                                   name="company_email" required>
                                                                                        </div>
                                                                                    </div>


                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Company Phone Number</label>
                                                                                            <input type="text" id="projectinput1" class="form-control" value="{{$data->company_phone}}"
                                                                                                    name="company_phone" required>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-md-12">
                                                                                        <img src="ActivityCoverImage/{{$data->activityimage}}" style="width: 100%;padding-bottom: 10px">
                                                                                    </div>
                                                                                </div>
                                                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Update</button>
                                                                            </form>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><a href="{{ route('backend.DeleteActivitySummary',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="summernote-edit-save" hidden>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Summernote Click to edit</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body">

                                                <div class="form-group">
                                                    <button id="edit" class="btn btn-primary" type="button"><i class="la la-pencil"></i> Edit</button>
                                                    <button id="save" class="btn btn-success" type="button"><i class="la la-save"></i> Save</button>
                                                </div>
                                                <div class="form-group">
                                                    <div class="summernote-edit">
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/pages/timeline.min.js"></script>
                    <script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>
                </div>
            </div>
        </div>
    </div>

@endsection
