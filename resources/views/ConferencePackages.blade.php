@extends('layouts.master')

@section('title', 'RTTA')

@section('content')

    @include('layouts.topmenu')
<style>
    .sf-button.accent {
        color: #fff;
        background-color: #6b442b;
        border-color: #6b442b;
    }
    .title-wrap .spb-heading>span {
        display: inline-block;
        text-transform: uppercase;
        font-family: "Open Sans";
        line-height: 28px;
        font-weight: 700;
        font-style: normal;
        font-size: 18px;
    }
    .spb-asset-content p {
        color: #000 !important;
    }
    p,span,b{
        font-family: "Open Sans" !important;
        line-height: 28px !important;
        font-style: normal !important;
        font-size: 15px !important;
    }
    h3, .single_variation_wrap .single_variation span.price, .sf-promo-bar p.standard, .sf-promo-bar.text-size-standard p, .sf-icon-box-animated-alt .front .back-title {
        font-family: "Open Sans";
        line-height: 28px;
        font-weight: 700;
        font-style: normal;
        font-size: 18px;
        text-transform: uppercase;
    }
    .woocommerce ul.products li.product .price {
        float: none;
        width: 100%;
        text-align: inherit;
        font-size: 14px;
        line-height: 20px;
        margin-top: 0;
        margin-bottom: 0;
        display: block;
        text-transform: uppercase;
    }
</style>
    <div id="sf-mobile-slideout-backdrop"></div>

    <div id="main-container" class="clearfix">
    @foreach($listconference as $conf)
    <div class="fancy-heading-wrap  fancy-style">
        <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('ConferenceImages/{{$conf->conference_image_name}}');" data-height="475" data-img-width="2000" data-img-height="800">
            <span class="media-overlay" style="background-color:transparent;opacity:0;"></span>
            <div class="heading-text container" data-textalign="left">
                <h1 class="entry-title">{{$conf->conference_title}}</h1>
            </div>
        </div>
    </div>
    @endforeach

        <div class="inner-container-wrap">
            <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
                <div class="clearfix">
                    <div class="page-content hfeed clearfix">
                        <div class="clearfix post-14975 page type-page status-publish hentry" id="14975">
                            <section data-header-style="" class="row fw-row  dynamic-header-change">
                                <div class="spb-row-container spb-row-full-width col-sm-12  col-natural" data-row-style="" data-v-center="true" data-top-style="none" data-bottom-style="none" style="padding-left:1%;padding-right:1%;margin-top:0px;margin-bottom:0px;">
                                    <div class="spb_content_element" style="padding-top:0px;padding-bottom:0px;">
                                        <section class="container ">
                                            <div class="row">
                                                <div class="blank_spacer col-sm-12" style="height:30px;"></div>
                                            </div>
                                        </section>
                                        <section data-header-style="" class="row fw-row  dynamic-header-change">
                                            <div class="spb-row-container spb-row-full-width col-sm-12  col-natural" data-row-style="" data-v-center="true" data-top-style="none" data-bottom-style="none" style="padding-left:1%;padding-right:1%;margin-top:0px;margin-bottom:0px;">
                                                <div class="spb_content_element" style="padding-top:0px;padding-bottom:0px;">

                                                    <section class="row fw-row ">
                                                        <div class="product_list_widget woocommerce spb_content_element col-sm-12">
                                                            <div class="spb-asset-content">
                                                                <div class="title-wrap clearfix container "></div>
                                                                <ul class="products list-latest-products row products-full-width multi-masonry-items product-type-standard " data-columns="4">
                                                                    <div class="clearfix product col-sm-3 grid-sizer"></div>

                                                                    @foreach($listpackages as $datas)
                                                                        <li class="qv-hover product-display-gallery details-align-left col-sm-3 product" data-width="col-sm-3">
                                                                            <figure class="animated-overlay product-transition-zoom">
                                                                                <div class="badge-wrap"></div>
                                                                                <div class="multi-masonry-img-wrap"><img src="PackageImages/{{$datas->package_image_name}}"  alt="" /></div>
                                                                                <div class="hover-price">
                                                                                    <span class="price"><span class="amount">{{$datas->package_title}} with {{$datas->company_name}}</span></span>
                                                                                </div>
                                                                                <a href="{{ route('MoreTourPackage',['id'=> $datas->id])}}"></a>
                                                                                <div class="figcaption-wrap"></div>
                                                                                <figcaption>
                                                                                    <div class="thumb-info">
                                                                                        {{--<h4>{{$datas->activitytitle}}</h4>--}}
                                                                                        {{--<h5 class="posted_in"><a href="" rel="tag">{{$datas->attraction_province}}</a></h5>--}}
                                                                                    </div>
                                                                                </figcaption>
                                                                            </figure>
                                                                        </li>
                                                                    @endforeach


                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </section>
                            <div class="link-pages"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sf-full-header-search-backdrop"></div>
</div>
    @include('layouts.footer')
@endsection