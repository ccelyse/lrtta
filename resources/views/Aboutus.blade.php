@extends('layouts.master')

@section('title', 'RTTA')

@section('content')
    <style>
        .spb-asset-content p {
            color: #000 !important;
        }
        .title-wrap .spb-heading>span {
            display: inline-block;
            text-transform: uppercase;
        }
        .team-member .team-member-name {
            margin-bottom: 0;
            font-size: 11px;
        }
        .team-member-position{
            line-height: 20px !important;
        }
    </style>
    @include('layouts.topmenu')

    <div id="sf-mobile-slideout-backdrop"></div>
    <div id="main-container" class="clearfix">
        <div class="fancy-heading-wrap  fancy-style">
            <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/8ahadada.jpg');" data-height="475" data-img-width="2000" data-img-height="800">
                <span class="media-overlay" style="background-color:#3c3b3b;opacity:0.5;"></span>
                <div class="heading-text container" data-textalign="left">
                    <h1 class="entry-title">About RTTA</h1>
                </div>
            </div>
        </div>
        <div class="inner-container-wrap">
            <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
                <div class="clearfix">
                    <div class="page-content hfeed clearfix">
                        <div class="clearfix post-13116 page type-page status-publish hentry" id="13116">
                            <section class="container ">
                                <div class="row">
                                    <div class="blank_spacer col-sm-12  " style="height:60px;"></div>
                                </div>
                            </section>
                            <section class="container ">
                                <div class="row">
                                    <div class="spb_content_element col-sm-6 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>About RTTA</span></h3>
                                            </div>
                                            <p><strong>RTTA</strong>, which stands for Rwanda Tours and Travel Association, was founded in the year 2000 by a small group of Rwandan Tours & Travel operators concerned about the quality of tourism services in the country. These founding members recognized the need to create awareness of Rwanda as a competent tourism destination as well as represent the interests of the national tours and travel industry.</p>
                                            <p>RTTA Members are the industry leaders in the tours and travel trade and represent a wide spectrum of travel and vacation packages available in Rwanda.</p>

                                        </div>
                                    </div>
                                    <div class="spb_content_element col-sm-6 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Mission</span></h3>
                                            </div>
                                            <p>To uphold ethical business practices among our members while ensuring sustainable development of the tourism industry in Rwanda.</p>
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Vision</span></h3>
                                            </div>
                                            <p>To preserve and protect our heritage and environment through integrated tourism development in Rwanda.</p>

                                        </div>
                                    </div>
                                    <div class="spb_content_element col-sm-12 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                           <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Objectives</span></h3>
                                            </div>
                                            <ul class="sf-list ">
                                                <li><i class="sf-icon-right-chevron"></i><span>To develop mechanisms for unification and loyalty of tour operators and travel agent’s interests in dynamic and effective way</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>To put in place amicable conflicts settlement and arbitration as well as assistance to the development of good and coherent business atmosphere</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>To ensure the representation of the tour operators and travel agents in the partnership between private, public sectors as well as Non Governmental Organisations</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>Improvement of member business knowledge and understanding and subsequent professional performance</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>To establish rules and regulations and recommend guidelines for new members in Rwanda’s tourism and travel industry</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>Maintain Rwanda’s good reputation as a tourist destination by making sure that tour operators and travel agents maintain high and professional ethics and standards and give value of services rendered to visitors</span></li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </section>


                            <div class="link-pages"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sf-full-header-search-backdrop"></div>
    </div>
@include('layouts.footer')
@endsection