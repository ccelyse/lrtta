@extends('layouts.master')

@section('title', 'RSGA')

@section('content')

    @include('layouts.topmenu')

    <div id="sf-mobile-slideout-backdrop"></div>
    <div id="main-container" class="clearfix">
        <div class="fancy-heading-wrap  fancy-style">
            <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/akagera.jpeg');" data-height="475" data-img-width="2000" data-img-height="800">
                <span class="media-overlay" style=""></span>
                <div class="heading-text container" data-textalign="left">
                    <h1 class="entry-title">PUBLICATIONS</h1>
                </div>
            </div>
        </div>
        <div class="inner-container-wrap">
            <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
                <div class="clearfix">
                    <div class="page-content hfeed clearfix">
                        <div class="clearfix post-13116 page type-page status-publish hentry" id="13116">
                            <section class="container ">
                                <div class="row">
                                    <div class="blank_spacer col-sm-12  " style="height:60px;"></div>
                                </div>
                            </section>
                            <section data-header-style="" class="row fw-row  dynamic-header-change">
                                <div class="spb-row-container spb-row-content-width col-sm-12 hidden-xs col-natural" data-row-style="" data-v-center="false" data-top-style="none" data-bottom-style="none" style="margin-top: 0%!important;margin-left: 0%!important;margin-right: 0%!important;margin-bottom: 0%!important;border-top: 0px default !important;border-left: 0px default !important;border-right: 0px default !important;border-bottom: 0px default !important;padding-top: 0%!important;padding-left: 0%!important;padding-right: 0%!important;padding-bottom: 0%!important;background-color:undefined!important;">
                                    <div class="spb_content_element" style="">
                                        <section class="container ">
                                            <div class="row">
                                                <div class="blank_spacer col-sm-12  " style="height:30px;"></div>
                                            </div>
                                        </section>
                                        <section class="container ">
                                            <div class="row">
                                                <p>The law and ministerial orders regulating the operations of all tourism entities in the country, including accommodation establishments, restaurants, bars, nightclubs, tour operators, travel agencies, tour guides, tourism information offices, cultural tourism entities (cultural villages, private museums) and any other entity as may be determined by Order of the Minister. </p>
                                                <ul class="sf-list ">
                                                    <li><i class="sf-icon-right-chevron"></i><span><strong><a href="Law_regulating_Tourism_industry.pdf" style="color:#006937 !important;font-weight: bold;" target="_blank">LAW REGULATING THE TOURISM INDUSTRY IN RWANDA</a></strong> </span></li>
                                                    <li><i class="sf-icon-right-chevron"></i><span><strong> <a href="Signed Ministerial Order on Licensing of Tourism Entities and Grading of Tourism Enties.pdf" style="color:#006937 !important;font-weight: bold;" target="_blank">MINISTERIAL ORDER DETERMINING THE REQUIREMENTS AND FEES FOR A TOURISM ENTITY TO BE GRANTED THE OPERATING LICENSE</a></strong> </span></li>

                                                </ul>
                                            </div>
                                        </section>

                                    </div>
                                </div>
                            </section>

                            <div class="link-pages"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sf-full-header-search-backdrop"></div>
    </div>
@include('layouts.footer')
@endsection