@extends('layouts.master')

@section('title', 'RTTA')

@section('content')

    @include('layouts.topmenu')

    <div id="sf-mobile-slideout-backdrop"></div>
<div id="main-container" class="clearfix">
    <div class="fancy-heading-wrap  fancy-style">
        <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/lakekivu2.jpg');" data-height="475" data-img-width="2000" data-img-height="800">
            <span class="media-overlay" style="background-color:transparent;opacity:0.5;"></span>
            <div class="heading-text container" data-textalign="left">
                <h1 class="entry-title">Visit Rwanda</h1>
            </div>
        </div>
    </div>

    <div class="inner-container-wrap">
        <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
            <div class="clearfix">
                <div class="page-content hfeed clearfix">
                    <div class="clearfix post-14975 page type-page status-publish hentry" id="14975">
                        <section data-header-style="" class="row fw-row  dynamic-header-change">
                            <div class="spb-row-container spb-row-full-width col-sm-12  col-natural" data-row-style="" data-v-center="true" data-top-style="none" data-bottom-style="none" style="padding-left:1%;padding-right:1%;margin-top:0px;margin-bottom:0px;">
                                <div class="spb_content_element" style="padding-top:0px;padding-bottom:0px;">
                                    <section class="container ">
                                        <div class="row">
                                            <div class="blank_spacer col-sm-12" style="height:30px;"></div>
                                        </div>
                                    </section>
                                    <section data-header-style="" class="row fw-row  dynamic-header-change">
                                        <div class="spb-row-container spb-row-full-width col-sm-12  col-natural" data-row-style="" data-v-center="true" data-top-style="none" data-bottom-style="none" style="padding-left:1%;padding-right:1%;margin-top:0px;margin-bottom:0px;">
                                            <div class="spb_content_element" style="padding-top:0px;padding-bottom:0px;">

                                                <section class="row fw-row ">
                                                    <div class="product_list_widget woocommerce spb_content_element col-sm-12">
                                                        <div class="spb-asset-content">
                                                            <div class="title-wrap clearfix container "></div>
                                                            <ul class="products list-latest-products row products-full-width multi-masonry-items product-type-standard " data-columns="4">
                                                                <div class="clearfix product col-sm-3 grid-sizer"></div>

                                                                @foreach($data as $datas)
                                                                <li class="qv-hover product-display-gallery details-align-left col-sm-3 product" data-width="col-sm-3">
                                                                    <figure class="animated-overlay product-transition-zoom">
                                                                        <div class="badge-wrap"></div>
                                                                        <div class="multi-masonry-img-wrap"><img src="https://visiteastafrica.travel/Destination/{{$datas->destinationcoverimage}}"  alt="" /></div>
                                                                        <div class="hover-price">
                                                                            <span class="price"><span class="amount">{{$datas->destinationtitle}}</span></span>
                                                                        </div>

                                                                        <a href="{{ route('AttractionsMore',['id'=> $datas->id])}}"></a>
                                                                        <div class="figcaption-wrap"></div>
                                                                        <figcaption>
                                                                            <div class="thumb-info">
                                                                                <h4>{{$datas->destinationtitle}}</h4>
                                                                                <h5 class="posted_in"><a href="" rel="tag">Rwanda</a></h5>
                                                                            </div>
                                                                        </figcaption>
                                                                    </figure>

                                                                </li>
                                                                @endforeach


                                                            </ul>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </section>
                        <div class="link-pages"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="sf-full-header-search-backdrop"></div>
</div>
    @include('layouts.footer')
@endsection