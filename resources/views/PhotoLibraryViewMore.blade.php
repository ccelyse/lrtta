@extends('layouts.master')

@section('title', 'RSGA')

@section('content')
    <style>
        .spb-asset-content p {
            color: #000 !important;
        }
        .title-wrap .spb-heading>span {
            display: inline-block;
            text-transform: uppercase;
        }
        html {
            line-height: 1.15;
            /* 1 */
            -ms-text-size-adjust: 100%;
            /* 2 */
            -webkit-text-size-adjust: 100%;
            /* 2 */
        }
        .photolibrary {
            position: absolute;
            text-align: center;
            left: 0;
            right: 0;
            bottom: 0;
            background: #006937;
            margin: 5px;
            color: #fff;
            font-size: 12px;
            text-transform: uppercase;
            z-index: 999;
            padding: 5px;
        }

    </style>
    <script>

    </script>
    @include('layouts.topmenu')

    <div id="sf-mobile-slideout-backdrop"></div>
    <div id="main-container" class="clearfix">
        @foreach($listcat as $data)
            <div class="fancy-heading-wrap  fancy-style">
                <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('photocategories/{{$data->categorypicture}}');" data-height="475" data-img-width="2000" data-img-height="800">
                    {{--<span class="media-overlay" style="background-color:#3c3b3b;opacity:0.5;"></span>--}}
                    <div class="heading-text container" data-textalign="left">
                        <h1 class="entry-title" style="text-transform: uppercase;">{{$data->categoryname}}</h1>
                    </div>
                </div>
            </div>
            <div class="inner-container-wrap">
                <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
                    <div class="clearfix">
                        <div class="page-content hfeed clearfix">
                            <div class="clearfix post-12577 page type-page status-publish hentry" id="12577">
                                <section class="row fw-row ">
                                    <div class="spb_portfolio_widget portfolio-wrap spb_content_element col-sm-12">
                                        <div class="spb-asset-content">
                                            <ul class="portfolio-items multi-masonry-items filterable-items col-3 row clearfix portfolio-full-width no-gutters thumbnail-default">
                                                <li class="clearfix portfolio-item col-sm-4 grid-sizer">

                                                @foreach($findpicture as $info)
                                                    <li itemscope itemtype="http://schema.org/CreativeWork" data-id="id-{{$info->id}}" class="clearfix portfolio-item col-sm-4 size-standard multi-masonry-item gallery-item  blender light ">
                                                        <div class="portfolio-item-wrap">
                                                            <h3 class="photolibrary" >{{$info->phototitle}}</h3>
                                                            <figure class="animated-overlay overlay-style">
                                                                <a href="MemberPhotoLibrary/{{$info->photoname}}" class="lightbox" data-rel="ilightbox[portfolio]"></a>
                                                                <div class="multi-masonry-img-wrap"><img itemprop="image" src="MemberPhotoLibrary/{{$info->photoname}}" alt="" /></div>
                                                                <div class="figcaption-wrap"></div>
                                                            </figure>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                                <div class="link-pages"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--<div class="inner-container-wrap">--}}
            {{--<div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">--}}
            {{--<div class="clearfix">--}}
            {{--<div class="page-content hfeed clearfix">--}}
            {{--<div class="clearfix post-13116 page type-page status-publish hentry" id="13116">--}}
            {{--<section class="container ">--}}
            {{--<div class="row">--}}
            {{--<div class="blank_spacer col-sm-12  " style="height:60px;"></div>--}}
            {{--</div>--}}
            {{--</section>--}}
            {{--<section class="container">--}}

            {{--<div class="row">--}}
            {{--@foreach($findpicture as $info)--}}
            {{--<a href="#">--}}
            {{--<div class="column">--}}
            {{--<h3 class="photolibrary" >{{$info->phototitle}}</h3>--}}
            {{--<img src="MemberPhotoLibrary/{{$info->photoname}}" class="img-responsive">--}}
            {{--</div>--}}
            {{--</a>--}}
            {{--@endforeach--}}
            {{--</div>--}}
            {{--</section>--}}
            {{--<div class="link-pages"></div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
        @endforeach
        <div id="sf-full-header-search-backdrop"></div>
    </div>
    @include('layouts.footer')
@endsection