@extends('layouts.master')

@section('title', 'RTTA')

@section('content')
    <!-- Primary Meta Tags -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('layouts.topmenu')
<style>
    /*#listing_pack{*/
    /*    height: auto !important;*/
    /*}*/
    .sf-list li {
        margin-bottom: 6px;
        line-height: 200%;
        text-transform: uppercase;
    }
    figure.animated-overlay figcaption {
        background: linear-gradient(to bottom,rgba(34, 37, 37, 0.6) 25%,rgb(24, 25, 25) 100%) !important;
    }
    .sf-list li {
        float: left;
        padding: 25px;
    }
</style>
    <div id="sf-mobile-slideout-backdrop"></div>
    <div id="main-container" class="clearfix">
        <div class="fancy-heading-wrap  fancy-style">
            <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/Rwanda-Lake-Kivu-1-1920x1439.jpg');" data-height="475" data-img-width="2000" data-img-height="800">
                <span class="media-overlay" style=""></span>
                <div class="heading-text container" data-textalign="left">
                    <h1 class="entry-title">Domestic Packages</h1>
                </div>
            </div>
        </div>
        <div class="inner-container-wrap">
            <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
                <div class="clearfix">
                    <div class="page-content hfeed clearfix">
                        <div class="clearfix post-13116 page type-page status-publish hentry" id="13116">
                            <section class="container ">
                                <div class="row">
                                    <div class="blank_spacer col-sm-12  " style="height:60px;"></div>
                                </div>
                            </section>
                            <section data-header-style="" class="row fw-row  dynamic-header-change">
                                <div class="spb-row-container spb-row-content-width col-sm-12 hidden-xs col-natural" data-row-style="" data-v-center="false" data-top-style="none" data-bottom-style="none" style="margin-top: 0%!important;margin-left: 0%!important;margin-right: 0%!important;margin-bottom: 0%!important;border-top: 0px default !important;border-left: 0px default !important;border-right: 0px default !important;border-bottom: 0px default !important;padding-top: 0%!important;padding-left: 0%!important;padding-right: 0%!important;padding-bottom: 0%!important;background-color:undefined!important;">
                                    <div class="spb_content_element" style="">
                                        <section class="container ">
                                            <div class="row">
                                                <div class="blank_spacer col-sm-12  " style="height:30px;"></div>
                                            </div>
                                        </section>
                                        <section class="container ">
                                            <div class="row">
                                               <ul class="sf-list ">
                                                   <li>
                                                       <i class="sf-icon-right-chevron"></i>
                                                       <span>
                                                            <strong>
                                                                <a style="color:#323232 !important;font-weight: bold;cursor: pointer;" onclick="All()">All</a>
                                                            </strong>
                                                        </span>
                                                   </li>
                                                    <li>
                                                        <i class="sf-icon-right-chevron"></i>
                                                        <span>
                                                            <strong>
                                                                <a style="color:#323232 !important;font-weight: bold;cursor: pointer;" onclick="Western()">Western</a>
                                                            </strong>
                                                        </span>
                                                    </li>
                                                    <li><i class="sf-icon-right-chevron"></i><span>
                                                            <strong>
                                                                <a style="color:#323232 !important;font-weight: bold;cursor: pointer;" onclick="Southwestern()">Southwestern</a>
                                                            </strong>
                                                        </span>
                                                    </li>
                                                    <li><i class="sf-icon-right-chevron"></i>
                                                        <span>
                                                            <strong>
                                                                <a style="color:#323232 !important;font-weight: bold;cursor: pointer;" onclick="Northern()">Northern</a>
                                                            </strong>
                                                        </span>
                                                    </li>
                                                   <li><i class="sf-icon-right-chevron"></i>
                                                        <span>
                                                            <strong>
                                                                <a style="color:#323232 !important;font-weight: bold;cursor: pointer;" onclick="Eastern()">Eastern</a>
                                                            </strong>
                                                        </span>
                                                    </li>
                                                   <li><i class="sf-icon-right-chevron"></i>
                                                       <span>
                                                            <strong>
                                                                <a style="color:#323232 !important;font-weight: bold;cursor: pointer;" onclick="KigaliCity()">Kigali City</a>
                                                            </strong>
                                                        </span>
                                                   </li>
                                                </ul>
                                            </div>
                                        </section>

                                    </div>
                                </div>
                            </section>
                            <section data-header-style="" class="row fw-row  dynamic-header-change" style="margin-top: 30px">
                                <div class="spb-row-container spb-row-full-width col-sm-12  col-natural" data-row-style="" data-v-center="true" data-top-style="none" data-bottom-style="none" style="padding-left:1%;padding-right:1%;margin-top:0px;margin-bottom:0px;">
                                    <div class="spb_content_element" style="padding-top:0px;padding-bottom:0px;">
                                        <section class="row fw-row ">
                                            <div class="product_list_widget woocommerce spb_content_element col-sm-12">
                                                <div class="spb-asset-content">
                                                    <div class="title-wrap clearfix container "></div>
                                                    <ul id="listing_pack" class="products list-latest-products row products-full-width multi-masonry-items product-type-standard" data-columns="4">
                                                        <div class="clearfix product col-sm-3 grid-sizer"></div>

                                                        <div id="displayModernss">
                                                            @foreach($data as $datas)
                                                                <li class="qv-hover product-display-gallery details-align-left col-sm-3 product" data-width="col-sm-3">
                                                                    <figure class="animated-overlay product-transition-zoom">
                                                                        <div class="badge-wrap"></div>
                                                                        <div class="multi-masonry-img-wrap"><img src="PackageImages/{{$datas->package_image_name}}"  alt="" /></div>
                                                                        <div class="hover-price">
                                                                            <span class="price"><span class="amount" style="color:#f9c93d">{{$datas->package_title}}</span></span>
                                                                        </div>

                                                                        <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#{{$datas->id}}myModal"></a>
                                                                        <div class="figcaption-wrap"></div>
                                                                        <figcaption>
                                                                            <div class="thumb-info">
                                                                                <h5 class="posted_in"><a href="" rel="tag">{{$datas->package_price}}</a></h5>
                                                                            </div>
                                                                        </figcaption>
                                                                    </figure>
                                                                </li>
                                                                <div class="modal" id="{{$datas->id}}myModal">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">

                                                                            <!-- Modal Header -->
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title">{{$datas->package_title}}</h4>
                                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                            </div>

                                                                            <div class="modal-body">
{{--                                                                                <img src="PackageImages/{{$datas->package_image_name}}"  alt="" />--}}
                                                                                <div class="thumb-info">
                                                                                    <h4><strong>{{$datas->package_price}}</strong></h4>
                                                                                    <p class="posted_in">{{$datas->package_description}}</p>
                                                                                    <a href="PackageImages/{{$datas->package_document}}" style="background: #000;padding: 10px;" target="_blank">Download Journey</a>
                                                                                </div>
                                                                            </div>

                                                                            <!-- Modal footer -->
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-danger" data-dismiss="modal" style="background: #000;color: #fff; border: none;">Close</button>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>

                                                        <div id="displayModern">
                                                            <img src="images/loading.gif" style="margin: 0 auto;display:none;" id="loading">
                                                        </div>
                                                        <!-- Button to Open the Modal -->


                                                        <!-- The Modal -->
                                                        <div class="modal" id="myModalv1">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">

                                                                    <!-- Modal Header -->
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="package_title"></h4>
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    </div>

                                                                    <!-- Modal body -->
                                                                    <div class="modal-body">
                                                                           <div class="thumb-info" id="thumb-info">
                                                                               <strong><h4 id="package_price"></h4></strong>
                                                                               <p class="posted_in" id="package_description"></p>
                                                                               <div id="package_down">
                                                                               </div>
                                                                           </div>
                                                                    </div>

                                                                    <!-- Modal footer -->
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-danger" data-dismiss="modal" style="background: #000;color: #fff; border: none;">Close</button>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>


                                                    </ul>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </section>

                            <div class="link-pages"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sf-full-header-search-backdrop"></div>
    </div>
    @include('layouts.footer')
{{--    <script--}}
{{--            src="https://code.jquery.com/jquery-3.3.1.min.js"--}}
{{--            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="--}}
{{--            crossorigin="anonymous"></script>--}}
    <script>
        jQuery(document).on('click', '.edit-modal', function() {
            var id = jQuery(this).data('id');
            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "DomesticPackagesById",
                data: {
                    'id': id,
                },
                success: function(response) {

                    JSON.stringify(response); //to string
                       // console.log(response);
                    jQuery('#thumb-info').show();
                    jQuery('#package_price').html(response[0].package_price);
                    jQuery('#package_description').html(response[0].package_description);
                    jQuery('#package_title').html(response[0].package_title);
                    jQuery('#package_down').html('<a href="PackageImages/'+ response[0].package_document +'" style="background: #000;padding: 10px;" target="_blank">Download Journey</a>');

                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                }
            });
        });

        function All() {
            jQuery(document).ready(function ($) {
                // document.getElementById("loading").style.display = "block";
                document.getElementById("displayModernss").style.display = "none";
                jQuery("#displayModernss").empty();
                jQuery("#displayModern").empty();
                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "DomesticPackagesByCategory",
                    data: {
                        'category': "All",
                    },
                    success: function(response) {
                        JSON.stringify(response); //to string
                        console.log(response);
                        // document.getElementById("loading").style.display = "none";
                        jQuery.each(response, function(index, standard){
                            jQuery('#displayModern').append('' +'' +
                                '<li class="col-md-3">' +
                                '<figure class="animated-overlay product-transition-zoom">' +
                                '<div class="badge-wrap"></div>' +
                                '<div class="multi-masonry-img-wrap"><img src="PackageImages/'+ standard.package_image_name +'"  alt="" /></div>' +
                                '<div class="hover-price">' +
                                '<span class="price"><span class="amount" style="color:#f9c93d"> '+ standard.package_title +'</span></span>' +
                                '</div>' +
                                '<a type="button" class="btn btn-primary edit-modal" data-toggle="modal" data-id="' + standard.id + '" data-target="#myModalv1"></a>' +
                                '<div class="figcaption-wrap"></div>' +
                                '<figcaption>' +
                                '<div class="thumb-info">' +
                                '<h5 class="posted_in">'+ standard.package_price +'</h5>' +
                                '</div>' +
                                '</figcaption>' +
                                '</figure>' +
                                '</li>'
                            );

                        });
                        // $("#displayModern").html(response);
                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            });

        }
        function Western() {
            jQuery(document).ready(function ($) {
                // document.getElementById("loading").style.display = "block";
                jQuery("#displayModernss").empty();
                document.getElementById("displayModernss").style.display = "none";
                jQuery("#displayModern").empty();

                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "DomesticPackagesByCategory",
                    data: {
                        'category': "Western",
                    },
                    success: function(response) {
                        JSON.stringify(response); //to string
                        console.log(response);
                        // document.getElementById("loading").style.display = "none";
                        jQuery.each(response, function(index, standard){
                            jQuery('#displayModern').append('' +'' +
                                '<li class="col-md-3">' +
                                '<figure class="animated-overlay product-transition-zoom">' +
                                '<div class="badge-wrap"></div>' +
                                '<div class="multi-masonry-img-wrap"><img src="PackageImages/'+ standard.package_image_name +'"  alt="" /></div>' +
                                '<div class="hover-price">' +
                                '<span class="price"><span class="amount" style="color:#f9c93d"> '+ standard.package_title +'</span></span>' +
                                '</div>' +
                                '<a type="button" class="btn btn-primary edit-modal" data-toggle="modal" data-id="' + standard.id + '" data-target="#myModalv1"></a>' +
                                '<div class="figcaption-wrap"></div>' +
                                '<figcaption>' +
                                '<div class="thumb-info">' +
                                '<h5 class="posted_in">'+ standard.package_price +'</h5>' +
                                '</div>' +
                                '</figcaption>' +
                                '</figure>' +
                                '</li>'
                            );

                        });
                        // $("#displayModern").html(response);
                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            });

        }
        function Northern() {
            jQuery(document).ready(function ($) {
                // document.getElementById("loading").style.display = "block";
                document.getElementById("displayModernss").style.display = "none";
                jQuery("#displayModernss").empty();
                jQuery("#displayModern").empty();

                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "DomesticPackagesByCategory",
                    data: {
                        'category': "Northern",
                    },
                    success: function(response) {
                        JSON.stringify(response); //to string
                        console.log(response);
                        // document.getElementById("loading").style.display = "none";
                        jQuery.each(response, function(index, standard){
                            jQuery('#displayModern').append('' +'' +
                                '<li class="col-md-3">' +
                                '<figure class="animated-overlay product-transition-zoom">' +
                                '<div class="badge-wrap"></div>' +
                                '<div class="multi-masonry-img-wrap"><img src="PackageImages/'+ standard.package_image_name +'"  alt="" /></div>' +
                                '<div class="hover-price">' +
                                '<span class="price"><span class="amount" style="color:#f9c93d"> '+ standard.package_title +'</span></span>' +
                                '</div>' +
                                '<a type="button" class="btn btn-primary edit-modal" data-toggle="modal" data-id="' + standard.id + '" data-target="#myModalv1"></a>' +
                                '<div class="figcaption-wrap"></div>' +
                                '<figcaption>' +
                                '<div class="thumb-info">' +
                                '<h5 class="posted_in">'+ standard.package_price +'</h5>' +
                                '</div>' +
                                '</figcaption>' +
                                '</figure>' +
                                '</li>'
                            );

                        });
                        // $("#displayModern").html(response);
                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            });

        }
        function Eastern() {
            jQuery(document).ready(function ($) {
                // document.getElementById("loading").style.display = "block";
                jQuery("#displayModernss").empty();
                document.getElementById("displayModernss").style.display = "none";
                jQuery("#displayModern").empty();

                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "DomesticPackagesByCategory",
                    data: {
                        'category': "Eastern",
                    },
                    success: function(response) {
                        JSON.stringify(response); //to string
                        console.log(response);
                        // document.getElementById("loading").style.display = "none";
                        jQuery.each(response, function(index, standard){
                            jQuery('#displayModern').append('' +'' +
                                '<li class="col-md-3">' +
                                '<figure class="animated-overlay product-transition-zoom">' +
                                '<div class="badge-wrap"></div>' +
                                '<div class="multi-masonry-img-wrap"><img src="PackageImages/'+ standard.package_image_name +'"  alt="" /></div>' +
                                '<div class="hover-price">' +
                                '<span class="price"><span class="amount" style="color:#f9c93d"> '+ standard.package_title +'</span></span>' +
                                '</div>' +
                                '<a type="button" class="btn btn-primary edit-modal" data-toggle="modal" data-id="' + standard.id + '" data-target="#myModalv1"></a>' +
                                '<div class="figcaption-wrap"></div>' +
                                '<figcaption>' +
                                '<div class="thumb-info">' +
                                '<h5 class="posted_in">'+ standard.package_price +'</h5>' +
                                '</div>' +
                                '</figcaption>' +
                                '</figure>' +
                                '</li>'
                            );

                        });
                        // $("#displayModern").html(response);
                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            });

        }
        function KigaliCity() {
            jQuery(document).ready(function ($) {
                // document.getElementById("loading").style.display = "block";
                jQuery("#displayModernss").empty();
                document.getElementById("displayModernss").style.display = "none";
                jQuery("#displayModern").empty();

                jQuery.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "DomesticPackagesByCategory",
                    data: {
                        'category': "Kigali City",
                    },
                    success: function(response) {
                        JSON.stringify(response); //to string
                        console.log(response);
                        // document.getElementById("loading").style.display = "none";
                        jQuery.each(response, function(index, standard){
                            jQuery('#displayModern').append('' +'' +
                                '<li class="col-md-3">' +
                                '<figure class="animated-overlay product-transition-zoom">' +
                                '<div class="badge-wrap"></div>' +
                                '<div class="multi-masonry-img-wrap"><img src="PackageImages/'+ standard.package_image_name +'"  alt="" /></div>' +
                                '<div class="hover-price">' +
                                '<span class="price"><span class="amount" style="color:#f9c93d"> '+ standard.package_title +'</span></span>' +
                                '</div>' +
                                '<a type="button" class="btn btn-primary edit-modal" data-toggle="modal" data-id="' + standard.id + '" data-target="#myModalv1"></a>' +
                                '<div class="figcaption-wrap"></div>' +
                                '<figcaption>' +
                                '<div class="thumb-info">' +
                                '<h5 class="posted_in">'+ standard.package_price +'</h5>' +
                                '</div>' +
                                '</figcaption>' +
                                '</figure>' +
                                '</li>'
                            );

                        });
                        // $("#displayModern").html(response);
                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            });

        }

    </script>
@endsection