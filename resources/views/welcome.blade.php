@extends('layouts.master')

@section('title', 'RTTA')

@section('content')
<style>
    .spb-asset-content p{
        color:#fff !important;
    }
    .title-wrap h3{
        color: #fff !important;
    }
    .owl-carousel .owl-item img {
        transform-style: preserve-3d;
        width: 200px;
    }

</style>
@include('layouts.topmenu')
<div id="sf-mobile-slideout-backdrop"></div>
<div class="swift-slider-outer">
    <div id="swift-slider-1" class="swift-slider swiper-container" data-type="slider" data-fullscreen="0" data-max-height="800" data-transition="slide" data-loop="true" data-slide-count="2" data-autoplay="" data-continue="0">
        <div class="swiper-wrapper">
            <div class="swiper-slide image-slide dynamic-header-change" data-slide-id="1" data-slide-title="Intro" style="background-image: url('images/Gorillas-1-compressor.jpg');background-color: #222222" data-bg-size="cover" data-bg-align="center" data-bg-horiz-align="center" data-mobile-bg-horiz-align="center" data-slide-img="http://uplift.swiftideas.com/wp-content/uploads/2014/01/uplift_slides_fence.jpg" data-style="light" data-header-style="light">
                <div class="overlay" style=""></div>
                <div class="caption-wrap container">
                    <div class="caption-content" data-caption-color="" data-caption-x="center" data-caption-y="middle" data-caption-size="smaller">
                        <div class="caption-excerpt">
                            <p>
                                <section class="row ">
                                    <div class="spb-animated-headline spb_content_element col-sm-12">
                                        <div class="spb-asset-content">
                                            <div class="sf-headline text-center zoom impact-text-large" style="color:#ffffff;">

                                                <span class="sf-words-wrapper">
                                                        <b class="is-visible">WILDLIFE</b>
                                                    <!--<b>quick</b>-->
                                          </span>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <br />
                                <a class="sf-button standard white default  sf-button-rounded sf-button-has-icon" href="{{url('VisitRwanda')}}"  target="_self"><span class="text">Visit Rwanda</span> <i class="fas fa-eye"></i></a>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide image-slide dynamic-header-change" data-slide-id="1" data-slide-title="Intro" style="background-image: url('images/footer-compressor.jpg');background-color: #222222" data-bg-size="cover" data-bg-align="center" data-bg-horiz-align="center" data-mobile-bg-horiz-align="center" data-slide-img="http://uplift.swiftideas.com/wp-content/uploads/2014/01/uplift_slides_fence.jpg" data-style="light" data-header-style="light">
                <div class="overlay"></div>
                <div class="caption-wrap container">
                    <div class="caption-content" data-caption-color="" data-caption-x="center" data-caption-y="middle" data-caption-size="smaller">
                        <div class="caption-excerpt">
                            <p>
                                <section class="row ">
                                    <div class="spb-animated-headline spb_content_element col-sm-12">
                                        <div class="spb-asset-content">
                                            <div class="sf-headline text-center zoom impact-text-large" style="color:#ffffff;">

                                                <span class="sf-words-wrapper">
                                          <b class="is-visible">ADVENTURE</b>
                                                    <!--<b>quick</b>-->
                                          </span>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <br />
                                <a class="sf-button standard white default  sf-button-rounded sf-button-has-icon" href="{{url('VisitRwanda')}}" target="_self"><span class="text">Visit Rwanda</span> <i class="fas fa-eye"></i></a>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="swiper-slide image-slide dynamic-header-change" data-slide-id="1" data-slide-title="Intro" style="background-image: url('images/Culture-1-compressor.jpg');background-color: #222222" data-bg-size="cover" data-bg-align="center" data-bg-horiz-align="center" data-mobile-bg-horiz-align="center" data-slide-img="http://uplift.swiftideas.com/wp-content/uploads/2014/01/uplift_slides_fence.jpg" data-style="light" data-header-style="light">
                <div class="overlay" style=""></div>
                <div class="caption-wrap container">
                    <div class="caption-content" data-caption-color="" data-caption-x="center" data-caption-y="middle" data-caption-size="smaller">
                        <div class="caption-excerpt">
                            <p>
                                <section class="row ">
                                    <div class="spb-animated-headline spb_content_element col-sm-12">
                                        <div class="spb-asset-content">
                                            <div class="sf-headline text-center zoom impact-text-large" style="color:#ffffff;">

                                                <span class="sf-words-wrapper">
                                          <b class="is-visible">HERITAGE & CULTURE</b>
                                                    <!--<b>quick</b>-->
                                          </span>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <br />
                                <a class="sf-button standard white default  sf-button-rounded sf-button-has-icon" href="{{url('VisitRwanda')}}" target="_self"><span class="text">Visit Rwanda</span> <i class="fas fa-eye"></i></a>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide image-slide dynamic-header-change" data-slide-id="1" data-slide-title="Intro" style="background-image: url('images/agr-compressed.jpg');background-color: #222222" data-bg-size="cover" data-bg-align="center" data-bg-horiz-align="center" data-mobile-bg-horiz-align="center" data-slide-img="http://uplift.swiftideas.com/wp-content/uploads/2014/01/uplift_slides_fence.jpg" data-style="light" data-header-style="light">
                <div class="overlay" style=""></div>
                <div class="caption-wrap container">
                    <div class="caption-content" data-caption-color="" data-caption-x="center" data-caption-y="middle" data-caption-size="smaller">
                        <div class="caption-excerpt">
                            <p>
                                <section class="row ">
                                    <div class="spb-animated-headline spb_content_element col-sm-12">
                                        <div class="spb-asset-content">
                                            <div class="sf-headline text-center zoom impact-text-large" style="color:#ffffff;">

                                                <span class="sf-words-wrapper">
                                          <b class="is-visible">COMMUNITY BASED TOURISM</b>
                                                    <!--<b>quick</b>-->
                                          </span>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <br />
                                <a class="sf-button standard white default  sf-button-rounded sf-button-has-icon" href="{{url('VisitRwanda')}}" target="_self"><span class="text">Visit Rwanda</span> <i class="fas fa-eye"></i></a>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide image-slide dynamic-header-change" data-slide-id="1" data-slide-title="Intro" style="background-image: url('images/unnamed.jpg');background-color: #222222" data-bg-size="cover" data-bg-align="center" data-bg-horiz-align="center" data-mobile-bg-horiz-align="center" data-slide-img="http://uplift.swiftideas.com/wp-content/uploads/2014/01/uplift_slides_fence.jpg" data-style="light" data-header-style="light">
                <div class="overlay" style=""></div>
                <div class="caption-wrap container">
                    <div class="caption-content" data-caption-color="" data-caption-x="center" data-caption-y="middle" data-caption-size="smaller">
                        <div class="caption-excerpt">
                            <p>
                                <section class="row ">
                                    <div class="spb-animated-headline spb_content_element col-sm-12">
                                        <div class="spb-asset-content">
                                            <div class="sf-headline text-center zoom impact-text-large" style="color:#ffffff;">

                                                <span class="sf-words-wrapper">
                                          <b class="is-visible">LAKESIDE RECREATION</b>
                                                    <!--<b>quick</b>-->
                                          </span>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <br />
                                <a class="sf-button standard white default  sf-button-rounded sf-button-has-icon" href="{{url('VisitRwanda')}}" target="_self"><span class="text">Visit Rwanda</span> <i class="fas fa-eye"></i></a>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="swiper-slide image-slide dynamic-header-change" data-slide-id="1" data-slide-title="Intro" style="background-image: url('images/45638138665_32f071a50d_k-compressor.jpg');background-color: #222222" data-bg-size="cover" data-bg-align="center" data-bg-horiz-align="center" data-mobile-bg-horiz-align="center" data-slide-img="http://uplift.swiftideas.com/wp-content/uploads/2014/01/uplift_slides_fence.jpg" data-style="light" data-header-style="light">
                <div class="overlay" style=""></div>
                <div class="caption-wrap container">
                    <div class="caption-content" data-caption-color="" data-caption-x="center" data-caption-y="middle" data-caption-size="smaller">
                        <div class="caption-excerpt">
                            <p>
                                <section class="row ">
                                    <div class="spb-animated-headline spb_content_element col-sm-12">
                                        <div class="spb-asset-content">
                                            <div class="sf-headline text-center zoom impact-text-large" style="color:#ffffff;">

                                                <span class="sf-words-wrapper">
                                          <b class="is-visible">MEETINGS & EVENTS</b>
                                                    <!--<b>quick</b>-->
                                          </span>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <br />
                                <a class="sf-button standard white default  sf-button-rounded sf-button-has-icon" href="{{url('VisitRwanda')}}" target="_self"><span class="text">Visit Rwanda</span> <i class="fas fa-eye"></i></a>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <a class="swift-slider-prev" href="index.php#">
            <svg version="1.1" class="svg-swift-slider-prev" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="48px" height="48px" viewBox="0 0 48 48" enable-background="new 0 0 48 48" xml:space="preserve">
                     <path fill="none" stroke="#222222" stroke-width="3" stroke-linecap="square" stroke-linejoin="round" stroke-miterlimit="10" d="
                        M14,24L34,4L14,24z" />
                <path fill="none" stroke="#222222" stroke-width="3" stroke-linecap="square" stroke-linejoin="round" stroke-miterlimit="10" d="
                        M14,24l20,20L14,24z" />
                  </svg>
            <h4>Previous</h4>
        </a>
        <a class="swift-slider-next" href="index.php#">
            <svg version="1.1" class="svg-swift-slider-next" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="48px" height="48px" viewBox="0 0 48 48" enable-background="new 0 0 48 48" xml:space="preserve">
                     <path fill="none" stroke="#222222" stroke-width="3" stroke-linecap="square" stroke-linejoin="round" stroke-miterlimit="10" d="
                        M34,24L14,44L34,24z" />
                <path fill="none" stroke="#222222" stroke-width="3" stroke-linecap="square" stroke-linejoin="round" stroke-miterlimit="10" d="
                        M34,24L14,4L34,24z" />
                  </svg>
            <h4>Next</h4>
        </a>
        <div class="swift-slider-pagination">
            <div class="dot"><span class=""></span></div>
            <div class="dot"><span class=""></span></div>
        </div>
        <div id="swift-slider-loader" class="circle">
            <div class="sf-svg-loader"><object data="wp-content/themes/uplift/images/loader-svgs/loader-32px-glyph_x-circle-08.svg" type="image/svg+xml"></object></div>
        </div>
    </div>
</div>
<div id="main-container" class="clearfix">
    <div class="inner-container-wrap">
        <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
            <div class="clearfix">
                <div class="page-content hfeed clearfix">
                    <div class="clearfix post-9 page type-page status-publish hentry" id="9">

                        <section data-header-style="" class="row fw-row  dynamic-header-change">
                            <div class="spb-row-container spb-row-content-width spb_parallax_asset sf-parallax parallax-content-height parallax-scroll spb_content_element bg-type-cover col-sm-12  col-natural">
                                <div class="spb_content_element" style="background: #f7f7f7;">
                                    <section class="container ">
                                        <div class="row">
                                            <div class="blank_spacer col-sm-12" style="height:60px;"></div>
                                        </div>
                                    </section>
                                    <section class="container" style="color: #000 !important;">
                                        <div class="row">
                                            <div class="spb_content_element col-sm-6 spb_text_column">
                                                <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                    <div class="title-wrap">
                                                        <h3 class="spb-heading spb-text-heading" style="color: #000 !important;"><span>Welcome to Rwanda!</span></h3>
                                                    </div>
                                                    <p style="color: #000 !important;"><strong>Rwanda</strong> is a landlocked East African country with a green mountainous landscape.
                                                        It has a temperate tropical highland climate, with lower temperatures than are
                                                        typical for equatorial countries due to its high elevation.</p>
                                                    <p style="color: #000 !important;">It is bordered by Uganda to the North, Tanzania to the East, Burundi to the South
                                                        and the Democratic Republic of Congo to the West.
                                                    </p>
                                                    <p style="color: #000 !important;"><strong>Rwanda</strong> is one of the safest countries in the world, ranked 1st in Africa and 5th globally by the World Economic Forum (WEF).</p>
                                                    <p style="color: #000 !important;"><strong>Rwanda</strong>  is also ranked 29th globally by the World Bank’s Doing Business 2019 report.</p>
                                                    <p style="color: #000 !important;"><strong>Rwanda</strong>  is open to everyone as a visa is issued on arrival for all citizens of the world. In the space of a week it is possible to experience many highlights thanks to the relatively compact size of our country. With a population of slightly over 12 million, Rwanda is geographically small and one of the world’s most densely populated countries.</p>
                                                    <p style="color: #000 !important;"><strong>Rwanda</strong>  is home to some of Africa’s most beautiful scenic views and is one of three countries in the world where visitors can see the majestic mountain gorillas – no longer critically endangered thanks to national conservation efforts.</p>



                                                </div>
                                            </div>
                                            <div class="spb_content_element col-sm-6 spb_text_column">
                                                <div class="spb-asset-content" style="padding-top:7%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                    {{--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1164466.6010107568!2d29.425530684800858!3d-1.989037217968997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x19c29654e73840e3%3A0x7490b026cbcca103!2sRwanda!5e0!3m2!1sen!2srw!4v1553201721809" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>--}}

                                                    <img src="images/map-compressor.png" style="width: 100%">
                                                    <img src="images/17781345620_be99cce7db_k-compressor.jpg" style="width: 100%">

                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section class="container ">
                                        <div class="row">
                                            <div class="blank_spacer col-sm-12  " style="height:40px;"></div>
                                        </div>
                                    </section>
                                </div>
                                <div class="row-overlay" style="background-color:#222222;opacity:0.9;"></div>
                            </div>
                        </section>
                        <section data-header-style="" class="row fw-row  dynamic-header-change spb_parallax_asset sf-parallax parallax-content-height parallax-scroll">
                            <div class="spb-row-container spb-row-content-width spb_parallax_asset sf-parallax parallax-content-height parallax-scroll spb_content_element bg-type-cover col-sm-12  col-natural">
                                <div class="spb_content_element" style="background-image: url(images/NH_OO_Activities_Chimpanzee_Trek_6571_MASTER-compressor-compressed.jpg);background-position: center;background-size: cover;">
                                    <section class="container ">
                                        <div class="row">
                                            <div class="blank_spacer col-sm-12" style="height:60px;"></div>
                                        </div>
                                    </section>
                                    <section class="container ">
                                        <div class="row">
                                            <div class="spb_content_element col-sm-6 spb_text_column">
                                                <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                    <div class="title-wrap">
                                                        <h3 class="spb-heading spb-text-heading"><span>Follow us on Facebook @Rwanda Tours and Travel Association</span></h3>
                                                    </div>
                                                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fweb.facebook.com%2Frttarwanda%2F&tabs=timeline&width=800&height=700&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=293883694598098" width="100%" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

                                                </div>
                                            </div>
                                            <div class="spb_content_element col-sm-6 spb_text_column">
                                                <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                    <div class="title-wrap">
                                                        <h3 class="spb-heading spb-text-heading" ><span>Follow us on Twitter @rttaRwanda</span></h3>

                                                    </div>

                                                    <a class="twitter-timeline" href="https://twitter.com/rttaRwanda?ref_src=twsrc%5Etfw" data-height="400">Tweets by rttaRwanda</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section class="container ">
                                        <div class="row">
                                            <div class="blank_spacer col-sm-12  " style="height:40px;"></div>
                                        </div>
                                    </section>
                                </div>
                                <div class="row-overlay" style="background-color:#222222;opacity:0.9;"></div>
                            </div>
                        </section>
                        <section data-header-style="" class="row fw-row  dynamic-header-change">
                            <div class="spb-row-container spb-row-content-width spb_parallax_asset sf-parallax parallax-content-height parallax-scroll spb_content_element bg-type-cover col-sm-12  col-natural">
                                <div class="spb_content_element" style="background-image: url(images/NH_OO_Activities_Waterfall_Trek_0199_MASTER-compressed.jpg);background-position: center;background-size: cover;">
                                    <section class="container ">
                                        <div class="row">
                                            <div class="blank_spacer col-sm-12" style="height:60px;"></div>
                                        </div>
                                    </section>
                                    <section class="row fw-row ">
                                        <div class="spb_portfolio_showcase_widget spb_content_element has-pagination col-sm-12">
                                            <a class="view-all hidden" href="portfolio/portfolio-3-column-standard/index.html"><i class="sf-icon-quickview"></i></a>
                                            <div class="spb-asset-content">
                                                <div class="title-wrap container">
                                                    <h3 class="spb-heading center-title" style="color: #fff !important;"><span>We are a member of</span></h3>
                                                </div>
                                                <div class="port-carousel carousel-wrap">
                                                    <a href="index.html#" class="carousel-prev"><i class="sf-icon-left-chevron"></i></a><a href="index.html#" class="carousel-next"><i class="sf-icon-right-chevron"></i></a>
                                                    <div id="carousel-1" class="portfolio-showcase carousel-items staged-carousel gutters clearfix" data-columns="5" data-auto="false" data-pagination="yes">
                                                        <div itemscope class="clearfix carousel-item portfolio-item gallery-item">
                                                            <figure class="animated-overlay overlay-style">
                                                                <a href="" class="link-to-post"></a>
                                                                <div class="img-wrap"><img itemprop="image" src="images/13.jpg" width="500" height="375" alt="" style="display: none !important;"/></div>
                                                                <div class="figcaption-wrap"></div>
                                                            </figure>
                                                        </div>
                                                        <div itemscope class="clearfix carousel-item portfolio-item gallery-item">
                                                            <figure class="animated-overlay overlay-style">
                                                                <a href="" class="link-to-url"></a>
                                                                <div class="img-wrap"><img itemprop="image" src="images/13.jpg" width="500" height="375" alt="" /></div>
                                                                <div class="figcaption-wrap"></div>
                                                            </figure>
                                                        </div>

                                                        <div itemscope class="clearfix carousel-item portfolio-item gallery-item" style="display: none !important;">
                                                            <figure class="animated-overlay overlay-style">
                                                                <a href="" class="link-to-url"></a>
                                                                <div class="img-wrap"><img itemprop="image" src="images/feder.png" width="500" height="375" alt="" /></div>
                                                                <div class="figcaption-wrap"></div>

                                                            </figure>
                                                        </div>
                                                        <div itemscope class="clearfix carousel-item portfolio-item gallery-item">
                                                            <figure class="animated-overlay overlay-style">
                                                                <a href="https://www.psf.org.rw/" class="link-to-post"></a>
                                                                <div class="img-wrap"><img itemprop="image" src="images/p3.jpg" width="500" height="375" alt="" /></div>
                                                                <div class="figcaption-wrap"></div>

                                                            </figure>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="spb-asset-content">
                                                <div class="title-wrap container">
                                                    <h3 class="spb-heading center-title" style="color: #fff !important;"><span>Our Partners</span></h3>
                                                </div>
                                                <div class="port-carousel carousel-wrap">
                                                    <a href="index.html#" class="carousel-prev"><i class="sf-icon-left-chevron"></i></a><a href="index.html#" class="carousel-next"><i class="sf-icon-right-chevron"></i></a>
                                                    <div id="carousel-2" class="portfolio-showcase carousel-items staged-carousel gutters clearfix" data-columns="5" data-auto="false" data-pagination="yes">
                                                        <div itemscope class="clearfix carousel-item portfolio-item gallery-item">
                                                            <figure class="animated-overlay overlay-style">
                                                                <a href="" class="link-to-post"></a>
                                                                <div class="img-wrap"><img itemprop="image" src="images/12.jpg" width="200" height="375" alt="" style="display: none !important;"/></div>
                                                                <div class="figcaption-wrap"></div>
                                                            </figure>
                                                        </div>
                                                        <div itemscope class="clearfix carousel-item portfolio-item gallery-item">
                                                            <figure class="animated-overlay overlay-style">
                                                                <a href="https://rdb.rw/" class="link-to-url"></a>
                                                                <div class="img-wrap"><img itemprop="image" src="images/12.jpg" width="200" height="375" alt="" /></div>
                                                                <div class="figcaption-wrap"></div>
                                                            </figure>
                                                        </div>

                                                        <div itemscope class="clearfix carousel-item portfolio-item gallery-item" style="display: none !important;">
                                                            <figure class="animated-overlay overlay-style">
                                                                <a href="" class="link-to-url"></a>
                                                                <div class="img-wrap"><img itemprop="image" src="images/p3.jpg" width="200" height="375" alt="" /></div>
                                                                <div class="figcaption-wrap"></div>

                                                            </figure>
                                                        </div>
                                                        <div itemscope class="clearfix carousel-item portfolio-item gallery-item">
                                                            <figure class="animated-overlay overlay-style">
                                                                <a href="https://www.rwandair.com/" class="link-to-post"></a>
                                                                <div class="img-wrap"><img itemprop="image" src="images/14.jpg" width="200" height="375" alt="" /></div>
                                                                <div class="figcaption-wrap"></div>

                                                            </figure>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section class="container ">
                                        <div class="row">
                                            <div class="blank_spacer col-sm-12  " style="height:40px;"></div>
                                        </div>
                                    </section>
                                </div>
                                <div class="row-overlay" style="background-color:#222222;opacity:0.9;"></div>
                            </div>
                        </section>
                        <section data-header-style="" class="row fw-row  dynamic-header-change" >
                            <div class="inner-container-wrap" style="background-image:url('images/20146915415_8e8aa69321_k.jpg') !important;background-size:cover ;">
                                <div class="inner-page-wrap has-no-sidebar no-top-spacing clearfix">

                                    <div class="clearfix">
                                        <div class="page-content hfeed clearfix">
                                            <div class="clearfix post-13072 page type-page status-publish hentry" id="13072">

                                                <section class="container "><div class="row"><div class="blank_spacer col-sm-12  " style="height:60px;"></div>
                                                    </div></section>
                                                <section class="container "><div class="row">
                                                        <div class="spb_content_element col-sm-5 spb_text_column">
                                                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15949.999335745973!2d30.1035248!3d-1.9533694!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc76b5e00aaa31b51!2sM%26M+Plaza!5e0!3m2!1sen!2srw!4v1530612631578" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                                                        </div>
                                                        <div class="spb-column-container col-sm-4" style="padding-left:15px; padding-right:15px; ">
                                                            <div class="spb-asset-content" style="">
                                                                <section class="container "><div class="row">
                                                                        <div class="spb_content_element col-sm-12 spb_text_column">
                                                                            <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                                                <div class="title-wrap"><h3 class="spb-heading spb-text-heading"><span>Office Contacts</span></h3></div>
                                                                                <p>Call: 250 788 332 220 <br />
                                                                                    Email: <a href="" class="__cf_email__" style="color:#fff;">info@rtta.rw</a><br>
                                                                                    <a href="" class="__cf_email__" style="color:#fff;">rttarwanda@gmail.com</a></p>
                                                                            </div>
                                                                        </div> </div></section>
                                                                <section class="container "><div class="row"><div class="blank_spacer col-sm-12  " style="height:30px;"></div>
                                                                    </div></section>
                                                                <section class="container "><div class="row">
                                                                        <div class="spb_content_element col-sm-12 spb_text_column">
                                                                            <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                                                <div class="title-wrap"><h3 class="spb-heading spb-text-heading"><span>Follow us @RTTA</span></h3></div>
                                                                                <ul class="social-icons standard ">
                                                                                    <li class="twitter"><a href="https://twitter.com/rttaRwanda" target="_blank"><i class="fa-twitter"></i><i class="fa-twitter"></i></a></li>
                                                                                    <li class="facebook"><a href="https://web.facebook.com/rttarwanda/" target="_blank"><i class="fa-facebook"></i><i class="fa-facebook"></i></a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div> </div></section>
                                                            </div>
                                                        </div>
                                                        <div class="spb-column-container col-sm-3" style="padding-left:15px; padding-right:15px; ">
                                                            <div class="spb-asset-content" style="">
                                                                <section class="container "><div class="row">
                                                                        <div class="spb_content_element col-sm-12 spb_text_column">
                                                                            <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                                                <div class="title-wrap"><h3 class="spb-heading spb-text-heading"><span>Our Address</span></h3></div>
                                                                                <p>Kigali,Rwanda<br />
                                                                                    Gishushu, KG 8 Avenue N.6<br />
                                                                                    M&M Plaza <br />
                                                                                    5th Floor</p>
                                                                            </div>
                                                                        </div> </div></section>
                                                            </div>
                                                        </div> </div></section>
                                                <div class="link-pages"></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="link-pages"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="sf-full-header-search-backdrop"></div>
</div>
@include('layouts.footer')
@endsection
