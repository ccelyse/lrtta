@extends('layouts.master')

@section('title', 'RTTA')

@section('content')

    @include('layouts.topmenu')
<style>
    .sf-button.accent {
        color: #fff;
        background-color: #6b442b;
        border-color: #6b442b;
    }
    .spb-asset-content{
        margin-top: 0px !important;
        margin-bottom: 0px !important;
    }
    .carousel-item {
        background: #252731 !important;
    }

    .left-part {
        width: 70%;
        background-size: cover;
        float: left;
    }

    .right-part {
        width: 30%;
        padding: 15px;
        float: right;
    }

    .right-part p {
        margin-top: 0px !important;
        margin-bottom: 0px !important;
        font-family: "Open Sans" !important;
        line-height: 28px !important;
        font-weight: 400 !important;
        font-style: normal !important;
        font-size: 15px !important;
        word-wrap: break-word;
        color: #fff !important;
    }
    .title{
        font-size: 18px;
        font-weight: 600;
        color: #ffffff;
        padding-left: 4px;
        border-left: 5px solid green;
        margin-bottom: 10px !important;
    }
    .thingstodo{
        font-size: 16px;
        font-weight: 100;
        color: #ffffff !important;
        display: -webkit-box;
        -webkit-line-clamp: 10;
        -webkit-box-orient: vertical;
        overflow: hidden;
    }
    .spb-asset-content {
        margin-top: 0px !important;
        margin-bottom: 0px !important;
        font-family: "Open Sans" !important;
        line-height: 28px !important;
        font-weight: 400 !important;
        font-style: normal !important;
        font-size: 15px !important;
    }
    .title-wrap h3 {
        color: #000 !important;
        font-family: "Open Sans" !important;
        line-height: 28px !important;
        font-weight: 700 !important;
        font-style: normal !important;
        font-size: 18px !important;
    }
</style>
    <link rel="stylesheet" type="text/css" media="screen" href="slide/css/bootstrap.min.css" />
    {{--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"--}}
            {{--crossorigin="anonymous"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <div id="sf-mobile-slideout-backdrop"></div>
    @foreach($datas as $data)
<div id="main-container" class="clearfix">
    <div class="fancy-heading-wrap  fancy-style">
        <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('https://visiteastafrica.travel/Destination/{{$data->destinationcoverimage}}');" data-height="475" data-img-width="2000" data-img-height="800">
            <span class="media-overlay" style="background-color:transparent;opacity:0.5;"></span>
            <div class="heading-text container" data-textalign="left">
                <h1 class="entry-title">{{$data->destinationtitle}}</h1>
            </div>
        </div>
    </div>

    <div class="inner-container-wrap">
        <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
            <div class="clearfix">
                <div class="page-content hfeed clearfix">
                    <div class="clearfix post-14975 page type-page status-publish hentry" id="14975">
                        <section data-header-style="" class="row fw-row  dynamic-header-change">
                            <div class="spb-row-container spb-row-full-width col-sm-12  col-natural" data-row-style="" data-v-center="true" data-top-style="none" data-bottom-style="none" style="padding-left:1%;padding-right:1%;margin-top:0px;margin-bottom:0px;">
                                <div class="spb_content_element" style="padding-top:0px;padding-bottom:0px;">
                                    <section class="container ">
                                        <div class="row">
                                            <div class="blank_spacer col-sm-12" style="height:30px;"></div>
                                        </div>
                                    </section>
                                    <section class="container ">
                                        <div class="row">
                                            <div class="spb_content_element col-sm-6 spb_text_column">
                                                <div class="title-wrap">
                                                    <h3 class="spb-heading spb-text-heading"><span>General Information</span></h3>
                                                </div>
                                                <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;margin-top:0px !important;margin-bottom: 0px !important;">
                                                    {{$data->destinationgeneralinformation}}
                                                </div>

                                            </div>
                                            <div class="spb_content_element col-sm-6 spb_text_column">
                                                <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                    <?php
                                                        $str = $data->destinationmap;
                                                        echo "$str";
                                                    ?>
                                                    {{--{{$data->destinationmap}}--}}
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="spb_content_element col-sm-11 spb_text_column">
                                                <div class="title-wrap">
                                                    <h3 class="spb-heading spb-text-heading"><span>Things To Do</span></h3>
                                                </div>
                                                <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                                        <ol class="carousel-indicators">
                                                        @foreach(array_slice($things, 0, 1) as $thingInds)
                                                            <li data-target="#carouselExampleIndicators" data-slide-to="{{$thingInds->id}}" class="active"></li>
                                                        @endforeach
                                                        @foreach(array_slice($things, 1, 15) as $thingInd)
                                                            <li data-target="#carouselExampleIndicators" data-slide-to="{{$thingInd->id}}"></li>

                                                        @endforeach
                                                        </ol>
                                                        <div class="carousel-inner">
                                                            @foreach(array_slice($things, 0, 1) as $thingss)
                                                                <div class="carousel-item active">
                                                                    <div class="left-part">
                                                                        <img src="https://visiteastafrica.travel/PlacesPictures/{{$thingss->coverimage}}" class="d-block w-100">
                                                                    </div>
                                                                    <div class="right-part">
                                                                        <h1 class="title">{{$thingss->placetitle}}</h1>
                                                                        <p class="thingstodo">{{$thingss->placegeneralinformation}}</p>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                            @foreach(array_slice($things, 1, 15) as $thing)
                                                            <div class="carousel-item">
                                                                <div class="left-part">
                                                                    <img src="https://visiteastafrica.travel/PlacesPictures/{{$thing->coverimage}}" class="d-block w-100" alt="...">
                                                                </div>
                                                                <div class="right-part">
                                                                    <h1 class="title">{{$thing->placetitle}}</h1>
                                                                    <p class="thingstodo">{{$thing->placegeneralinformation}}</p>
                                                                </div>
                                                            </div>
                                                            @endforeach
                                                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                                    <span class="sr-only">Previous</span>
                                                                </a>
                                                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                                    <span class="sr-only">Next</span>
                                                                </a>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>



                                        </div>
                                    </section>
                                </div>
                            </div>
                        </section>
                        <div class="link-pages"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="sf-full-header-search-backdrop"></div>
</div>
<script src="slide/js/bootstrap.min.js"></script>
@endforeach
    @include('layouts.footer')
@endsection