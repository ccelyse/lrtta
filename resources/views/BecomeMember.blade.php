@extends('layouts.master')

@section('title', 'RTTA')

@section('content')

    @include('layouts.topmenu')
    <style>
        .help-block{
            color: red !important;
        }
        .title-wrap h3 {
            color: #000 !important;
        }
        .spb-asset-content p {
            color: #000 !important;
        }
        .woocommerce form .form-row select {
            cursor: pointer;
            margin: 0;
            float: right;
            width: 75%;
        }
        .woocommerce form .form-row select {
            cursor: pointer;
            margin: 0;
            float: right;
            width: 60% !important;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <div id="sf-mobile-slideout-backdrop"></div>
    <div id="main-container" class="clearfix">
        <div class="fancy-heading-wrap  fancy-style">
            <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/19768195949_44169f866c_k.jpg');" data-height="475" data-img-width="2000" data-img-height="800">
                <span class="media-overlay" style="background-color:#3c3b3b;opacity:0;"></span>
                <div class="heading-text container" data-textalign="left">
                    <h1 class="entry-title">MEMBERSHIP</h1>
                </div>
            </div>
        </div>
        <div class="inner-container-wrap">
            <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
                <div class="clearfix">
                    <div class="page-content hfeed clearfix">
                        <div class="clearfix post-13116 page type-page status-publish hentry" id="13116">
                            <section class="container ">
                                <div class="row">
                                    <div class="blank_spacer col-sm-12" style="height:60px;"></div>
                                </div>
                            </section>
                            <section class="container ">
                                <div class="row">
                                    <div class="spb_content_element col-sm-10   col-md-offset-1 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>REQUIREMENTS</span></h3>
                                            </div>
                                            <p>Membership application is open to companies registered in Rwanda to offer tourism and travel services with the aims of encouraging quality, professional and responsible services in Rwanda.</p>
                                            <p><strong>Definition:</strong> Tour operators are businesses that combine two or more travel services such as transport, accommodation, meals, entertainment, sightseeing and any other tourism related services to tourists as a single product (called a tour package).</p>
                                            <ul class="sf-list ">
                                                <li><i class="sf-icon-right-chevron"></i><span>To submit a duly filled membership application form </span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>To submit a copy of your company registration certificate issued by RDB</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>To have a Tax Identification Number (TIN)</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>To have at least two permanent staff running the office and submit submit the CVs of the staff</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span></span>To have at least one 4WD tourist vehicle or a contract for such vehicle rental services with a supplier </li>
                                                <li><i class="sf-icon-right-chevron"></i><span></span>To have a website</li>
                                                <li><i class="sf-icon-right-chevron"></i><span></span>To have a physical address and signage to your office , the office should have sufficient space for reception and easy access to washrooms</li>
                                                <li><i class="sf-icon-right-chevron"></i><span>To have a risk mitigation mechanism in place</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>To pay a non-refundable once-off registration fee:  $250 and annual membership fee: $1000</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="spb_content_element col-sm-10   col-md-offset-1 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>WHY JOIN US? </span></h3>
                                            </div>
                                            <p>RTTA members are connected to a vast business network that spans the tourism and travel globe. Membership is your connection to the packaged tourism and travel industry providing member companies with the tools needed to learn, link, network and grow.</p>
                                        </div>
                                    </div>

                                    <div class="spb_content_element col-sm-10   col-md-offset-1 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>BENEFITS </span></h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="spb_content_element col-sm-6 text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <ul class="sf-list ">
                                                <li><i class="sf-icon-right-chevron"></i><span>Getting listed in the RTTA tours and travel members directory on the RTTA website and Rwanda Tourism Guide</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>RTTA Membership gives you the right to use RTTA logo as a sign of reliability, professionalism and quality relating to your service users and increased possibility to establish business relations with reliable companies</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>Trade referral and representation to Government, RDB – Tourism and Conservation, Rwanda Chamber of Tourism, Rwanda Private Sector Federation, regional and international tours and travel affiliate websites</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>Membership Certificate, which can be used to negotiate special member discounts/rates within tourism and travel industry</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>Representation of your needs and concerns to the industry’s senior levels of decision making for advocacy</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>Facilitation and information sharing on regional and international tourism and travel shows</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>Linking capacity development agencies for staff training in customer care and other needed training facilitation</span></li>
                                                <li><i class="sf-icon-right-chevron"></i>Information access on latest tourism and travel industry research and database and other matters of concern<span></span></li>

                                            </ul>
                                        </div>
                                    </div>

                                    <div class="spb_content_element col-sm-6 text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <ul class="sf-list ">
                                                <li><i class="sf-icon-right-chevron"></i><span>Networking with a wider tourism and travel professionals for good tourism and travel practice</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>As part of regular information and at your request, you will receive free advice about tourism and travel from members who are experts in tourism and travel</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>RTTA will follow up on issues affecting the business of members, integrate initiatives and proposals of all members and intervene for them with relevant ministries, bodies, institutions, associations and companies</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>You will be invited to participate in RTTA Annual General Assembly meeting, to the meetings of tours and travel sector working groups and/or national tourism working group as well as travel committees of your interest. In this way you will be able to engage RTTA for your interest too. Together with discussion and extending or confirming your knowledge from professional and business practice, you will get new business contacts, exchange ideas, experience and learn how other RTTA members, global tourism and travel professionals operate.</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>You and your staff will be able to attend RTTA´s seminars and lectures by tourism and travel experts</span></li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </section>
                            <section class="container ">
                                <div class="row">
                                    <div class="divider-wrap col-sm-12">
                                        <div class="spb_divider thin spb_content_element " style="margin-top: 30px; margin-bottom: 60px;"></div>
                                    </div>
                                </div>
                            </section>

                            <div class="link-pages"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sf-full-header-search-backdrop"></div>
    </div>

    @include('layouts.footer')
@endsection