@extends('layouts.master')

@section('title', 'RTTA')

@section('content')
    <style>
        .spb-asset-content p {
            color: #000 !important;
        }
        .title-wrap .spb-heading>span {
            display: inline-block;
            text-transform: uppercase;
        }
        .team-member .team-member-name {
            margin-bottom: 0;
            font-size: 11px;
        }
        .team-member-position{
            line-height: 20px !important;
        }
    </style>
    @include('layouts.topmenu')

    <div id="sf-mobile-slideout-backdrop"></div>
    <div id="main-container" class="clearfix">
        <div class="fancy-heading-wrap  fancy-style">
            <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/9TeaEstate3.jpg');" data-height="475" data-img-width="2000" data-img-height="800">
                <span class="media-overlay" style="background-color:#3c3b3b;opacity:0;"></span>
                <div class="heading-text container" data-textalign="left">
                    <h1 class="entry-title">About Rwanda</h1>
                </div>
            </div>
        </div>
        <div class="inner-container-wrap">
            <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
                <div class="clearfix">
                    <div class="page-content hfeed clearfix">
                        <div class="clearfix post-13116 page type-page status-publish hentry" id="13116">
                            <section data-header-style="" class="row fw-row  dynamic-header-change">
                                <div class="spb-row-container spb-row-content-width spb_parallax_asset sf-parallax parallax-content-height parallax-scroll spb_content_element bg-type-cover col-sm-12  col-natural">
                                    <div class="spb_content_element" style="background:#fff">
                                        <section class="container ">
                                            <div class="row">
                                                <div class="blank_spacer col-sm-12" style="height:60px;"></div>
                                            </div>
                                        </section>
                                        <section class="container ">
                                            <div class="row">
                                                <div class="team_list carousel-asset spb_content_element col-sm-12">
                                                    <div class="spb-asset-content">
                                                        <div class="title-wrap clearfix">
                                                            <h4 class="spb-heading" style="color: #fff !important;"><span>Rwanda Map</span></h4>
                                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1164466.6010107568!2d29.425530684800858!3d-1.989037217968997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x19c29654e73840e3%3A0x7490b026cbcca103!2sRwanda!5e0!3m2!1sen!2srw!4v1553201721809" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </section>

                                    </div>
                                </div>
                            </section>
                            <section class="container ">
                                <div class="row">
                                    <div class="blank_spacer col-sm-12  " style="height:60px;"></div>
                                </div>
                            </section>

                            <section class="container ">
                                <div class="row">
                                    <div class="spb_content_element col-sm-6 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>About Rwanda</span></h3>
                                            </div>
                                            <p>Rwanda is a landlocked country situated in Central Africa, bordered to the North by Uganda , to the East by Tanzania , by Burundi to the South and by the Democratic Republic of Congo to the West.</p>
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Geography</span></h3>
                                            </div>
                                            <p><strong>Area:</strong> 26,338 sq. km</p>
                                            <p><strong>Cities:</strong> Capital – Kigali (est. pop. 1,000,000).</p>
                                            <p><strong>Terrain:</strong> mostly grassy uplands and hills; relief is mountainous with altitude declining from west to east.</p>
                                            <p><strong>Climate:</strong> Rwanda has a temperate climate with two rainy seasons (February to April and November to January), mild in the mountains</p>

                                        </div>
                                    </div>

                                    <div class="spb_content_element col-sm-6 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Getting there</span></h3>
                                            </div>
                                            <p>Kigali :  Main gateway through Kigali International Airport served by 7 international airlines .</p>
                                            <img src="images/ds.png" style="width: 100%">
                                            {{--<p>All international flights arrive at Kigali International Airport, just 10 Km from Kigali city centre. International flights that operate to Kigali are Turkish Airlines, Qatar Airways, SN Brussels, Kenya Airways, Ethiopian Airlines and Rwandair Express.</p>--}}
                                            {{--<p>There are two direct flights from Brussels to Kigali by SN Brussels Airlines. Rwandair Express has introduced two flights a day from Nairobi (Kenya) and Entebbe (Uganda), and two per week from Johannesburg. Rwandair also has daily flights to Bujumbura and 3 flights weekly to Kilimanjaro. Rwandair Express currently flies thrice a week to Rubavu (Gisenyi-Rwanda) every Tuesday, Friday and Sunday.</p>--}}
                                            {{--<div class="title-wrap">--}}
                                                {{--<h3 class="spb-heading spb-text-heading"><span>Language</span></h3>--}}
                                            {{--</div>--}}
                                            {{--<p>Spoken languages are Kinyarwanda, French and English. French is widely spoken throughout the country. In the capital and other tourist centres, English is widely spoken.</p>--}}

                                        </div>
                                    </div>
                                    <div class="spb_content_element col-sm-12 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                           <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Money</span></h3>
                                            </div>
                                            <p>The unit of currency is the Rwanda franc. The US dollar is the hard currency of preference. It may be impossible to exchange traveler’s cheques away from the capital. Credit cards are accepted.</p>
                                        </div>
                                    </div>
                                </div>
                            </section>


                            <div class="link-pages"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sf-full-header-search-backdrop"></div>
    </div>
@include('layouts.footer')
@endsection