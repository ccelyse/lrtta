@extends('layouts.master')

@section('title', 'RTTA')

@section('content')
    <style>
        .spb-asset-content p {
            color: #000 !important;
        }
        .title-wrap .spb-heading>span {
            display: inline-block;
            text-transform: uppercase;
        }
        .team-member .team-member-name {
            margin-bottom: 0;
            font-size: 11px;
        }
        .team-member-position{
            line-height: 20px !important;
        }
    </style>
    @include('layouts.topmenu')

    <div id="sf-mobile-slideout-backdrop"></div>
    <div id="main-container" class="clearfix">
        <div class="fancy-heading-wrap  fancy-style">
            <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/8ahadada.jpg');" data-height="475" data-img-width="2000" data-img-height="800">
                <span class="media-overlay" style="background-color:#3c3b3b;opacity:0.5;"></span>
                <div class="heading-text container" data-textalign="left">
                    <h1 class="entry-title">Meet Our Executive Committe</h1>
                </div>
            </div>
        </div>
        <div class="inner-container-wrap">
            <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
                <div class="clearfix">
                    <div class="page-content hfeed clearfix">
                        <div class="clearfix post-13116 page type-page status-publish hentry" id="13116">
                            <section class="container ">
                                <div class="row">
                                    <div class="blank_spacer col-sm-12  " style="height:60px;"></div>
                                </div>
                            </section>

                            <section data-header-style="" class="row fw-row  dynamic-header-change">
                                <div class="spb-row-container spb-row-content-width spb_parallax_asset sf-parallax parallax-content-height parallax-scroll spb_content_element bg-type-cover col-sm-12  col-natural">
                                    <div class="spb_content_element" style="background-image: url(images/NY4.jpg);background-position: center;background-size: cover;">
                                        <section class="container ">
                                            <div class="row">
                                                <div class="blank_spacer col-sm-12" style="height:60px;"></div>
                                            </div>
                                        </section>
                                        <section class="container ">
                                            <div class="row">
                                                <div class="team_list carousel-asset spb_content_element col-sm-12">
                                                    <div class="spb-asset-content">
                                                        <div class="title-wrap clearfix">

                                                            <div class="carousel-arrows"><a href="index.php#" class="carousel-prev"><i class="sf-icon-left-chevron"></i></a><a href="index.php#" class="carousel-next"><i class="sf-icon-right-chevron"></i></a></div>
                                                        </div>
                                                        <div class="team-carousel carousel-wrap">
                                                            <div id="carousel-1" class="team-members carousel-items display-type-standard-alt clearfix" data-columns="4" data-auto="false">
                                                                <div itemscope data-id="id-0" class="clearfix team-member carousel-item">
                                                                    <div class="team-member-item-wrap">
                                                                        <figure class="animated-overlay">
                                                                            <a class="team-member-link " href="" data-id="212"></a><img itemprop="image" src="images/Bonita.jpg" width="270" height="270" alt="Bonita A. MUTONI" />
                                                                            <figcaption class="team-standard-alt">
                                                                                <div class="thumb-info thumb-info-alt">
                                                                                    <i>
                                                                                        <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />
                                                                                            <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />
                                                                                            <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />
                                                               </svg>
                                                                                    </i>
                                                                                </div>
                                                                            </figcaption>
                                                                        </figure>
                                                                        <div class="team-member-details-wrap">
                                                                            <h4 class="team-member-name"><a href="" class="" data-id="212">Bonita A. MUTONI</a></h4>
                                                                            <h5 class="team-member-position">Chairperson</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div itemscope data-id="id-1" class="clearfix team-member carousel-item">
                                                                    <div class="team-member-item-wrap">
                                                                        <figure class="animated-overlay">
                                                                            <a class="team-member-link " href="" data-id="213"></a><img itemprop="image" src="images/Oreste.jpg" width="270" height="270" alt="Mr. Oreste Ntirenganya" />
                                                                            <figcaption class="team-standard-alt">
                                                                                <div class="thumb-info thumb-info-alt">
                                                                                    <i>
                                                                                        <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />
                                                                                            <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />
                                                                                            <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />
                                                               </svg>
                                                                                    </i>
                                                                                </div>
                                                                            </figcaption>
                                                                        </figure>
                                                                        <div class="team-member-details-wrap">
                                                                            <h4 class="team-member-name"><a href="" class="" data-id="213">Mr. Oreste Ntirenganya</a></h4>
                                                                            <h5 class="team-member-position">Vice in Charge of Tour Operators</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div itemscope data-id="id-2" class="clearfix team-member carousel-item">
                                                                    <div class="team-member-item-wrap">
                                                                        <figure class="animated-overlay">
                                                                            <a class="team-member-link " href="" data-id="214"></a><img itemprop="image" src="images/Chetan.jpg" width="270" height="270" alt="Chetan GINA" />
                                                                            <figcaption class="team-standard-alt">
                                                                                <div class="thumb-info thumb-info-alt">
                                                                                    <i>
                                                                                        <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />
                                                                                            <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />
                                                                                            <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />
                                                               </svg>
                                                                                    </i>
                                                                                </div>
                                                                            </figcaption>
                                                                        </figure>
                                                                        <div class="team-member-details-wrap">
                                                                            <h4 class="team-member-name"><a href="" class="" data-id="214">Chetan GINA </a></h4>
                                                                            <h5 class="team-member-position">Vice in Charge of Travel Agencies</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div itemscope data-id="id-3" class="clearfix team-member carousel-item">
                                                                    <div class="team-member-item-wrap">
                                                                        <figure class="animated-overlay">
                                                                            <a class="team-member-link " href="" data-id="215"></a><img itemprop="image" src="images/Sifa.jpg" width="270" height="270" alt="Rosette KIRONGORO" />
                                                                            <figcaption class="team-standard-alt">
                                                                                <div class="thumb-info thumb-info-alt">
                                                                                    <i>
                                                                                        <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />
                                                                                            <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />
                                                                                            <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />
                                                               </svg>
                                                                                    </i>
                                                                                </div>
                                                                            </figcaption>
                                                                        </figure>
                                                                        <div class="team-member-details-wrap">
                                                                            <h4 class="team-member-name"><a href="" class="" data-id="215">SIFA UWERA</a></h4>
                                                                            <h5 class="team-member-position">RTTA Secretary</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div itemscope data-id="id-4" class="clearfix team-member carousel-item">
                                                                    <div class="team-member-item-wrap">
                                                                        <figure class="animated-overlay">
                                                                            <a class="team-member-link " href="" data-id="222"></a><img itemprop="image" src="images/Eugene-2.jpg" width="270" height="270" alt="Eugene NSHIMIYIMANA" />
                                                                            <figcaption class="team-standard-alt">
                                                                                <div class="thumb-info thumb-info-alt">
                                                                                    <i>
                                                                                        <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />
                                                                                            <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />
                                                                                            <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />
                                                               </svg>
                                                                                    </i>
                                                                                </div>
                                                                            </figcaption>
                                                                        </figure>
                                                                        <div class="team-member-details-wrap">
                                                                            <h4 class="team-member-name"><a href="" class="" data-id="222">Eugene NSHIMIYIMANA</a></h4>
                                                                            <h5 class="team-member-position">Treasurer</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </section>

                                    </div>
                                </div>
                            </section>

                            <div class="link-pages"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sf-full-header-search-backdrop"></div>
    </div>
@include('layouts.footer')
@endsection