@extends('layouts.master')

@section('title', 'RTTA')

@section('content')

    @include('layouts.topmenu')
    <div id="sf-mobile-slideout-backdrop"></div>
    <div id="main-container" class="clearfix">
        <div class="fancy-heading-wrap  fancy-style">
            <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/17781345620_be99cce7db_k.jpg');" data-height="475" data-img-width="2000" data-img-height="800">
                <span class="media-overlay" style=""></span>
                <div class="heading-text container" data-textalign="left">
                    <h1 class="entry-title">Confirmation</h1>
                </div>
            </div>
        </div>
        <div class="inner-container-wrap">
            <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
                <div class="clearfix">
                    <div class="page-content hfeed clearfix">
                        <div class="clearfix post-13116 page type-page status-publish hentry" id="13116">
                            <section class="container ">
                                <div class="row">
                                    <div class="blank_spacer col-sm-12  " style="height:60px;"></div>
                                </div>
                            </section>
                            <section class="container ">
                                <div class="row">
                                    <div class="spb_content_element col-sm-6 spb_text_column col-lg-offset-3">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">

                                            <p>Dear <strong><?php echo $name ?></strong>,<br> Thank you for Booking with us.<br>Our team will get back to you as soon as possible.</p>

                                        </div>
                                    </div>
                                </div>
                            </section>
                            <div class="link-pages"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sf-full-header-search-backdrop"></div>
    </div>
    @include('layouts.footer')
@endsection