@extends('layouts.master')

@section('title', 'RTTA')

@section('content')
    <style>
        .spb-asset-content p {
            color: #000 !important;
        }
        .title-wrap .spb-heading>span {
            display: inline-block;
            text-transform: uppercase;
        }
        .team-member .team-member-name {
            margin-bottom: 0;
            font-size: 11px;
        }
        .team-member-position{
            line-height: 20px !important;
        }
        a, .ui-widget-content a {
            color: #03634c;
        }
    </style>
    @include('layouts.topmenu')

    <div id="sf-mobile-slideout-backdrop"></div>
    <div id="main-container" class="clearfix">
        <div class="fancy-heading-wrap  fancy-style">
            <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/3095866280_a3064dd52a_b.jpg');" data-height="475" data-img-width="2000" data-img-height="800">
                <span class="media-overlay" style="background-color:#3c3b3b;opacity:0;"></span>
                <div class="heading-text container" data-textalign="left">
                    <h1 class="entry-title">Visa Information</h1>
                </div>
            </div>
        </div>
        <div class="inner-container-wrap">
            <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
                <div class="clearfix">
                    <div class="page-content hfeed clearfix">
                        <div class="clearfix post-13116 page type-page status-publish hentry" id="13116">

                            <section class="container ">
                                <div class="row">
                                    <div class="blank_spacer col-sm-12  " style="height:60px;"></div>
                                </div>
                            </section>

                            <section class="container ">
                                <div class="row">
                                    <div class="spb_content_element col-sm-12 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <p>Foreign Nationals wishing to obtain Rwandan Visas, submit their applications to Rwandan Embassies or Diplomatic Missions of their Country of residence for processing. Countries where we don’t have an Embassy or Diplomatic Mission, they should request for an entry facility on line which will be used to obtain a visa on any official point of entry in Rwanda. With  bilateral agreements, nationals of the following Countries may visit Rwanda without a visa for a period of up to 90 days: Hong Kong, Philippines, Mauritius, Singapore and the Democratic Republic of Congo (DRC).</p>
                                        </div>
                                    </div>

                                    <div class="spb_content_element col-sm-6 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Categories of Visas</span></h3>
                                            </div>
                                            <p><strong>Entry visa:</strong> available online only, issued for a single entry on any official point of entry in Rwanda, valid for 15 days and renewable by the Directorate General of Immigration and Emigration up to 90 days maximum</p>
                                            <p><strong>Transit visa:</strong> duration 2 days</p>
                                            <p><strong>Visitors’/ tourist visa:</strong> issued by Rwandan Embassies and renewable at Migration office</p>
                                            <p><strong>Resident visa with work permit/business-working:</strong> renewable and multiple entry</p>
                                            <p>Resident visa without work permit</p>
                                            <p><strong>Permanent visa:</strong> multiple entries</p>
                                        </div>
                                    </div>

                                    <div class="spb_content_element col-sm-6 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Visa Fees</span></h3>
                                            </div>
                                            <p>Entry visa – $30.00 – on arrival (no prior application required) for nationals of Australia, Germany, Israel, New Zealand, Rep of South Africa, Sweden, UK and USA.</p>

                                            <p>Tourist visa (T1) – free – (no prior application required) for nationals of Singapore, Hong Kong, Mauritius and Philippines <a href="https://www.migration.gov.rw/index.php?id=112">(https://www.migration.gov.rw/index.php?id=112)</a></p>

                                            <p>Tourist visa (T2) – $50.00 – must apply At the Directorate General of Immigration and Emigration or at Rwandan Embassy or Diplomatic Mission. <a href="https://www.migration.gov.rw/index.php?id=115">(https://www.migration.gov.rw/index.php?id=115)</a></p>
                                        </div>
                                    </div>

                                    <div class="spb_content_element col-sm-12 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>General conditions to obtain rwandan visas can obtained on the following link</span></h3>
                                            </div>
                                            <p><a href="https://www.migration.gov.rw/index.php?id=203">https://www.migration.gov.rw/index.php?id=203</a></p>
                                            <p>Foreigners with work/resident permits in Rwanda, Uganda and Kenya can travel within the 3 countries without paying visa fees. Eligible foreign residents are issued with interstate pass on exiting the host state & visitor’s pass endorsed in their passports on arrival. They are required to present valid (for at least 6 months) work/resident permits to get the interstate pass.</p>
                                            <p><strong>NB:</strong> Police clearance from country of residence within the last six months is required when applying for resident visa.</p>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <div class="link-pages"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sf-full-header-search-backdrop"></div>
    </div>
@include('layouts.footer')
@endsection