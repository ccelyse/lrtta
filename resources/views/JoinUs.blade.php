@extends('layouts.master')

@section('title', 'RTTA')

@section('content')

    @include('layouts.topmenu')
    <style>
        .help-block{
            color: red !important;
        }
        .title-wrap h3 {
            color: #000 !important;
        }
        .spb-asset-content p {
            color: #000 !important;
        }
        .woocommerce form .form-row select {
            cursor: pointer;
            margin: 0;
            float: right;
            width: 75%;
        }
        .woocommerce form .form-row select {
            cursor: pointer;
            margin: 0;
            float: right;
            width: 60% !important;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <div id="sf-mobile-slideout-backdrop"></div>
    <div id="main-container" class="clearfix">
        <div class="fancy-heading-wrap  fancy-style">
            <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/cars.jpeg');" data-height="475" data-img-width="2000" data-img-height="800">
                <span class="media-overlay" style="background-color:#3c3b3b;opacity:0;"></span>
                <div class="heading-text container" data-textalign="left">
                    <h1 class="entry-title">Join Us</h1>
                </div>
            </div>
        </div>
        <div class="inner-container-wrap">
            <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
                <div class="clearfix">
                    <div class="page-content hfeed clearfix">
                        <div class="clearfix post-13116 page type-page status-publish hentry" id="13116">
                            <section class="container ">
                                <div class="row">
                                    <div class="blank_spacer col-sm-12" style="height:60px;"></div>
                                </div>
                            </section>
                            <section class="container ">
                                <div class="row">
                                    <div class="spb_content_element col-sm-10   col-md-offset-1 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>PLEASE READ THE FOLLOWING INFORMATION CAREFULLY</span></h3>
                                            </div>
                                           <ul class="sf-list ">
                                                <li><i class="sf-icon-right-chevron"></i><span>Membership and registration fees have to be remitted to the RTTA Bank Account in ACCESS BANK: 1002100200621201(USD)</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>Attach a copy of the Company’s / Business Registration Certificate issued by RDB.</span></li>
                                                <li><i class="sf-icon-right-chevron"></i><span>Attach staff CVs</span></li>
                                           </ul>
                                            <p>RTTA will conduct a visit to your office upon submission of your application.</p>
                                            <p>Please note that the information you provide in this form will be treated as confidential.</p>
                                        </div>
                                    </div>

                                    <div class="spb_content_element col-sm-7   col-md-offset-1 spb_text_column">
                                        <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                            <div class="title-wrap">
                                                <h3 class="spb-heading spb-text-heading"><span>Apply for membership</span></h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="woocommerce">

                                        <form name="checkout" method="post" class="checkout woocommerce-checkout row" action="{{url('JoinMembers')}}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="container"></div>
                                            <div class="col-sm-10 col-md-offset-1" id="customer_details">
                                                <div>
                                                    <div class="woocommerce-billing-fields">
                                                        <h3>Join</h3>
                                                        <div class="clear"></div>

                                                        <p class="form-row form-row form-row-wide address-field update_totals_on_change validate-required" id="billing_country_field">
                                                            <label for="billing_country" class="">Type of Membership</label>
                                                            <select class="country_to_state country_select billing_country" name="typeofmembership"  required>
                                                                <option value="{{ old('typeofmembership') }}">{{ old('typeofmembership') }}</option>
                                                                <option value="Association" selected>Association</option>
                                                                <option value="Champions (Indashyikirwa)">Champions (Indashyikirwa)</option>
                                                                <option value="District">District</option>
                                                                <option value="Golden Circle">Golden Circle</option>

                                                            </select>
                                                        </p>

                                                        <p class="form-row form-row form-row-wide address-field update_totals_on_change validate-required" id="billing_country_field" >
                                                            <label for="billing_country" class="">Chamber</label>
                                                            <select class="country_to_state country_select billing_country" name="chamber" required>
                                                                <option value="{{ old('chamber') }}">{{ old('chamber') }}</option>
                                                                <option value="Chamber of Tourism" selected>Chamber of Tourism</option>
                                                                <option value="ChamberofAgricultureandlivestock">Chamber of Agriculture and livestock</option>
                                                                <option value="Chamber of Arts Crafts and Artisans">Chamber of Arts Crafts and Artisans</option>
                                                                <option value="Chamber of Commerce and Services">Chamber of Commerce and Services</option>
                                                                <option value="Chamber of Financial Institutions">Chamber of Financial Institutions</option>
                                                                <option value="Chamber of ICT">Chamber of ICT</option>
                                                                <option value="Chamber of Industry">Chamber of Industry</option>
                                                                <option value="Chamber of Liberal Profession">Chamber of Liberal Profession</option>
                                                                <option value="Chamber of Women Entrepreneurs">Chamber of Women Entrepreneurs</option>
                                                                <option value="Chamber of Young Entrepreneurs">Chamber of Young Entrepreneurs</option>

                                                            </select>
                                                        </p>
                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Company Name</label>
                                                            <input type="text" class="input-text" name="companyname" id="billing_last_name"  placeholder="" value="{{ old('companyname') }}" required/>
                                                        </p>

                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Company Code/Tin number</label>
                                                            <input type="text" class="input-text" name="companycode" id="billing_last_name" placeholder="" value="{{ old('companycode') }}" required/>
                                                        </p>

                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Business Category</label>
                                                            <select class="billing_country country_to_state country_select " name="gender" value="{{ old('gender') }}" required>
                                                                <option value="{{ old('chamber') }}">{{ old('chamber') }}</option>
                                                                <option value="Domestic">Domestic</option>
                                                                <option value="OutBound">OutBound</option>
                                                            </select>
                                                        </p>
                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Number of Cars</label>
                                                            <input type="text" class="input-text" name="numberofrooms" id="billing_last_name" placeholder="" value="{{ old('numberofrooms') }}" />
                                                        </p>


                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Gender</label>
                                                            <!--															<input type="text" class="input-text" name="gender" id="billing_last_name" placeholder="Gender" value="" />-->
                                                            <select class="billing_country country_to_state country_select " name="gender" value="{{ old('gender') }}" required>
                                                                <option value="{{ old('chamber') }}">{{ old('chamber') }}</option>
                                                                <option value="Male">Male</option>
                                                                <option value="Female">Female</option>
                                                            </select>
                                                        </p>

                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Phone number</label>
                                                            <input type="text" class="input-text" name="phonenumber" id="billing_last_name" placeholder="" value="{{ old('phonenumber') }}" required/>
                                                        </p>


                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">P.O Box</label>
                                                            <input type="text" class="input-text" name="pobx" id="billing_last_name" placeholder="" value="{{ old('pobx') }}" />
                                                        </p>

                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Email Address</label>
                                                            <input type="text" class="input-text" name="email" id="billing_last_name" placeholder="" value="{{ old('email') }}" required/>
                                                        </p>

                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Website</label>
                                                            <input type="text" class="input-text" name="website" id="billing_last_name" placeholder="" value="{{ old('website') }}" />
                                                        </p>

                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Business Address Street Name</label>
                                                            <input type="text" class="input-text" name="businessaddress" id="billing_last_name" placeholder="" value="{{ old('businessaddress') }}" required/>
                                                        </p>


                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Building name</label>
                                                            <input type="text" class="input-text" name="buildingname" id="billing_last_name" placeholder="" value="{{ old('buildingname') }}" required/>
                                                        </p>


                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Business Area(Quartier)</label>
                                                            <input type="text" class="input-text" name="businessarea" id="billing_last_name" placeholder="" value="{{ old('businessarea') }}" required/>
                                                        </p>


                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Province</label>
                                                            <select class="country_to_state country_select billing_country" name="province" required>
                                                                <option value="{{ old('province') }}">{{ old('province') }}</option>
                                                                <option value="City of Kigali">City of Kigali</option>
                                                                <option value="Eastern Province">Eastern Province</option>
                                                                <option value="Western Province">Western Province</option>
                                                                <option value="Southern Province">Southern Province</option>
                                                                <option value="Northern Province">Northern Province</option>
                                                            </select>
                                                        </p>


                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">District</label>
                                                            <select class="country_to_state country_select billing_country" name="district" required>
                                                                <option value="{{ old('district') }}">{{ old('district') }}</option>
                                                                <option value="Bugesera">Bugesera</option>
                                                                <option value="Burera">Burera</option>
                                                                <option value="Gakenke">Gakenke</option>
                                                                <option value="Gasabo">Gasabo</option>
                                                                <option value="Gatsibo">Gatsibo</option>
                                                                <option value="Gicumbi">Gicumbi</option>
                                                                <option value="Gisagara">Gisagara</option>
                                                                <option value="Huye">Huye</option>
                                                                <option value="Kamonyi">Kamonyi</option>
                                                                <option value="Karongi">Karongi</option>
                                                                <option value="Kayonza">Kayonza</option>
                                                                <option value="Kicukiro">Kicukiro</option>
                                                                <option value="Kirehe">Kirehe</option>
                                                                <option value="Muhanga">Muhanga</option>
                                                                <option value="Musanze">Musanze</option>
                                                                <option value="Ngoma">Ngoma</option>
                                                                <option value="Ngororero">Ngororero</option>
                                                                <option value="Nyabihu">Nyabihu</option>
                                                                <option value="Nyagatare">Nyagatare</option>
                                                                <option value="Nyamagabe">Nyamagabe</option>
                                                                <option value="Nyamasheke">Nyamasheke</option>
                                                                <option value="Nyanza">Nyanza</option>
                                                                <option value="Nyarugenge">Nyarugenge</option>
                                                                <option value="Nyaruguru">Nyaruguru</option>
                                                                <option value="Rubavu">Rubavu</option>
                                                                <option value="Ruhango">Ruhango</option>
                                                                <option value="Rulindo">Rulindo</option>
                                                                <option value="Rusuzi">Rusizi</option>
                                                                <option value="Rutsiro">Rutsiro</option>
                                                                <option value="Rwamagana">Rwamagana</option>
                                                            </select>
                                                        </p>

                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Sector</label>
                                                            <input type="text" class="input-text" name="sector" id="billing_last_name" placeholder="" value="{{ old('sector') }}" required/>
                                                        </p>

                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Cell</label>
                                                            <input type="text" class="input-text" name="cell" id="billing_last_name" placeholder="" value="{{ old('cell') }}" required/>
                                                        </p>

                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Company Type</label>
                                                            <select class="billing_country country_to_state country_select " name="companytype" required>
                                                                <option value="{{ old('companytype') }}">{{ old('companytype') }}</option>
                                                                <option value="Cooperative">Cooperative</option>
                                                                <option value="NGO">NGO</option>
                                                                <option value="Partly state-owned company">Partly state-owned company</option>
                                                                <option value="Private Company">Private Company</option>
                                                                <option value="Public Company">Public Company</option>
                                                                <option value="Sole Trader">Sole Trader</option>
                                                            </select>
                                                        </p>


                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Ownership</label>
                                                            <select class="billing_country country_to_state country_select" name="ownership"  required>
                                                                <option value="{{ old('ownership') }}">{{ old('ownership') }}</option>
                                                                <option value="100% foreign-owned">100% foreign-owned</option>
                                                                <option value="100% rwandan-owned">100% rwandan-owned</option>
                                                                <option value="Franchise">Franchise</option>
                                                                <option value="Joint venture (Foreign & Rwandan)">Joint venture (Foreign & Rwandan)</option>
                                                                <option value="Representation of a foreign company">Representation of a foreign company</option>
                                                                <option value="Other">Other</option>
                                                            </select>
                                                        </p>

                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Business Activity</label>
                                                            <select class="billing_country country_to_state country_select " name="businessactivity" required>
                                                                <option value="{{ old('businessactivity') }}">{{ old('businessactivity') }}</option>
                                                                <option value="Accounting Services">Accounting Services</option>
                                                                <option value="Adventure Tours">Adventure Tours</option>
                                                                <option value="Advertising Services">Advertising Services</option>
                                                                <option value="Agriculture Inputs Dealers">Agriculture Inputs Dealers</option>
                                                                <option value="Agro-Processing">Agro-Processing</option>
                                                                <option value="Architect">Architect</option>
                                                                <option value="Auto Driving School">Auto Driving School</option>
                                                                <option value="Bank services">Bank services</option>
                                                                <option value="Bank services">Bank services</option>
                                                                <option value="Beauty Makers">Beauty Makers</option>
                                                                <option value="Beverage producer">Beverage producer</option>
                                                                <option value="Beverage producer">Beverage producer</option>
                                                                <option value="Broadcasting">Broadcasting</option>
                                                                <option value="Business Service">Business Service</option>
                                                                <option value="Business-Plan Consulting">Business-Plan Consulting</option>
                                                                <option value="Car Rental Agency">Car Rental Agency</option>
                                                                <option value="Catering Services">Catering Services</option>
                                                                <option value="Cleaning service">Cleaning service</option>
                                                                <option value="Construction service">Construction service</option>
                                                                <option value="Consultancy services">Consultancy services</option>
                                                                <option value="Contractor">Contractor</option>
                                                                <option value="Creative industry">Creative industry</option>
                                                                <option value="Decoration">Decoration</option>
                                                                <option value="Dry Cleaning & Laundry Services">Dry Cleaning & Laundry Services</option>
                                                                <option value="E-commerce‎">E-commerce‎</option>
                                                                <option value="Education">Education</option>
                                                                <option value="Electrical Services">Electrical Services</option>
                                                                <option value="Energy">Energy</option>
                                                                <option value="Engineering service">Engineering service</option>
                                                                <option value="Event Services">Event Services</option>
                                                                <option value="Farming">Farming</option>
                                                                <option value="Fashion industry‎ ">Fashion industry‎ </option>
                                                                <option value="Financial services‎">Financial services‎</option>
                                                                <option value="Fitness Center">Fitness Center</option>
                                                                <option value="Forex Bureaus">Forex Bureaus</option>
                                                                <option value="Freight Forwarders">Freight Forwarders</option>
                                                                <option value="Freight transport">Freight transport</option>
                                                                <option value="Garage services">Garage services</option>
                                                                <option value="Hair Salon">Hair Salon</option>
                                                                <option value="Health Services">Health Services</option>
                                                                <option value="Hospitality services">Hospitality services</option>
                                                                <option value="Insurance Broker">Insurance Broker</option>
                                                                <option value="Insurance service">Insurance service</option>
                                                                <option value="Internet providers">Internet providers</option>
                                                                <option value="Language Translation">Language Translation</option>
                                                                <option value="Leather workers">Leather workers</option>
                                                                <option value="Manufacturing">Manufacturing</option>
                                                                <option value="Marketing services">Marketing services</option>
                                                                <option value="Media industry‎">Media industry‎</option>
                                                                <option value="Metal Workers">Metal Workers</option>
                                                                <option value="Mining">Mining</option>
                                                                <option value="Online services‎">Online services‎</option>
                                                                <option value="Paint and coatings industry‎">Paint and coatings industry‎</option>
                                                                <option value="Petroleum transport">Petroleum transport</option>
                                                                <option value="Photography Services">Photography Services</option>
                                                                <option value="Practice of law‎ ">Practice of law‎ </option>
                                                                <option value="Printing Services">Printing Services</option>
                                                                <option value="Processing">Processing</option>
                                                                <option value="Professional services">Professional services</option>
                                                                <option value="Property Valuers service">Property Valuers service</option>
                                                                <option value="Public Auditor">Public Auditor</option>
                                                                <option value="Public transport">Public transport</option>
                                                                <option value="Real estate and construction">Real estate and construction</option>
                                                                <option value="Real Estate Brokers ">Real Estate Brokers </option>
                                                                <option value="Repair Services">Repair Services</option>
                                                                <option value="Restaurant Delivery Service">Restaurant Delivery Service</option>
                                                                <option value="Retailer">Retailer</option>
                                                                <option value="Safari Guide">Safari Guide</option>
                                                                <option value="Security Services">Security Services</option>
                                                                <option value="Seller">Seller</option>
                                                                <option value="Sewing and Tailoring">Sewing and Tailoring</option>
                                                                <option value="Shipping Services">Shipping Services</option>
                                                                <option value="Software Applications Developer">Software Applications Developer</option>
                                                                <option value="Tax Services">Tax Services</option>
                                                                <option value="Tour Operator">Tour Operator</option>
                                                                <option value="Tourism‎">Tourism‎</option>
                                                                <option value="Transport‎">Transport‎</option>
                                                                <option value="Travel Agencies">Travel Agencies</option>
                                                                <option value="Waste management">Waste management</option>
                                                                <option value="Web-Site Services">Web-Site Services</option>
                                                                <option value="Wholesaler">Wholesaler</option>
                                                                <option value="Wood worker">Wood worker</option>
                                                            </select>
                                                        </p>



                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Export/Import Goods/Services</label>
                                                            <select class="billing_country country_to_state country_select " name="export">
                                                                <option value="{{ old('export') }}">{{ old('export') }}</option>
                                                                <option value="beverage">beverage</option>
                                                                <option value="coffee">coffee</option>
                                                                <option value="cars">cars</option>
                                                                <option value="electronic equipment">electronic equipment</option>
                                                                <option value="food products">food products</option>
                                                                <option value="gas">gas</option>
                                                                <option value="hardware">hardware</option>
                                                                <option value="horticulture">horticulture</option>
                                                                <option value="oil">oil</option>
                                                                <option value="pharmaceutical products">pharmaceutical products</option>
                                                                <option value="services">services</option>
                                                                <option value="spare parts">spare parts</option>
                                                                <option value="stationary">stationary</option>
                                                                <option value="Tea">Tea</option>
                                                            </select>
                                                        </p>



                                                        <!--<p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">-->
                                                        <!--<label for="billing_last_name" class="">Exporting Countries</label>-->
                                                        <!--<input type="text" class="input-text" name="billing_last_name" id="billing_last_name" placeholder="Exporting Countries" value="" />-->
                                                        <!--</p>-->



                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Number of Employees (Permanent)</label>
                                                            <select  class="billing_country country_to_state country_select " name="numberofemployees"  required>
                                                                <option value="{{ old('numberofemployees') }}">{{ old('numberofemployees') }}</option>
                                                                <option value="0">0</option>
                                                                <option value="From 1 to 5">From 1 to 5</option>
                                                                <option value="From 6  to 10">From 6  to 10</option>
                                                                <option value="From 11 to 20">From 11 to 20</option>
                                                                <option value="From 21 to 50">From 21 to 50</option>
                                                                <option value="From 51 to 100">From 51 to 100</option>
                                                                <option value="From 101 to 200">From 101 to 200</option>
                                                                <option value="From 201 to 500">From 201 to 500</option>
                                                                <option value="From 501 to 1000">From 501 to 1000</option>
                                                                <option value="More than 1000">More than 1000</option>
                                                            </select>
                                                        </p>

                                                        <p class="form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Number of Employees (Part time)</label>
                                                            <select  class="billing_country country_to_state country_select " name="numberofemployeesapart" required>
                                                                <option value="{{ old('numberofemployeesapart') }}">{{ old('numberofemployeesapart') }}</option>
                                                                <option value="0">0</option>
                                                                <option value="From 1 to 5">From 1 to 5</option>
                                                                <option value="From 6  to 10">From 6  to 10</option>
                                                                <option value="From 11 to 20">From 11 to 20</option>
                                                                <option value="From 21 to 50">From 21 to 50</option>
                                                                <option value="From 51 to 100">From 51 to 100</option>
                                                                <option value="From 101 to 200">From 101 to 200</option>
                                                                <option value="From 201 to 500">From 201 to 500</option>
                                                                <option value="From 501 to 1000">From 501 to 1000</option>
                                                                <option value="More than 1000">More than 1000</option>
                                                            </select>
                                                        </p>

                                                        <p class="form-group{{ $errors->has('fileToUpload') ? ' has-error' : '' }} form-row form-row form-row-last validate-required" id="billing_last_name_field">
                                                            <label for="billing_last_name" class="">Attach your company registration certificate issued by RDB (only PDF is accepted)</label>
                                                            <input type="file" class="input-text" name="fileToUpload" id="billing_last_name" placeholder="" value="" required/>
                                                            @if ($errors->has('fileToUpload'))
                                                                <span class="help-block">
                                                         <strong>{{ $errors->first('fileToUpload') }}</strong>
                                                    </span>
                                                            @endif
                                                        </p>


                                                        <p class="form-row form-row notes"><input type="submit" value="Send Message" class="wpcf7-form-control wpcf7-submit"  style="margin: 0px !important;"/></p>
                                                    </div>

                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                            <section class="container ">
                                <div class="row">
                                    <div class="divider-wrap col-sm-12">
                                        <div class="spb_divider thin spb_content_element " style="margin-top: 30px; margin-bottom: 60px;"></div>
                                    </div>
                                </div>
                            </section>

                            <div class="link-pages"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sf-full-header-search-backdrop"></div>
    </div>

    @include('layouts.footer')
@endsection