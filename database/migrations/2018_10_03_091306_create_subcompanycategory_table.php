<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubcompanycategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcompanycategory', function (Blueprint $table) {
            $table->increments('id');
            $table->string('companycategory');
            $table->string('subcompanycategory');
            $table->string('seatingcapacity');
            $table->string('numberofroom');
            $table->string('Grade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcompanycategory');
    }
}
