<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToActivitySummaryCompanyInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ActivitySummary', function($table) {
            $table->string('company_name');
            $table->string('company_email');
            $table->string('company_phone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ActivitySummary', function($table) {
            $table->string('company_name');
            $table->string('company_email');
            $table->string('company_phone');
        });
    }
}
