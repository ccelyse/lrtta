<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomesticPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domestic_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('package_title');
            $table->string('package_image_name');
            $table->longText('package_description');
            $table->string('package_price');
            $table->string('package_category');
            $table->string('package_document');
            $table->longText('company_name');
            $table->longText('company_email');
            $table->longText('company_phone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domestic_packages');
    }
}
