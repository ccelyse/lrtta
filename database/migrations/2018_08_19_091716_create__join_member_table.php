<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJoinMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('JoinMember', function (Blueprint $table) {
            $table->increments('id');
            $table->string('typeofmembership');
            $table->string('chamber');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('tinnumber');
            $table->string('telephonenumber');
            $table->string('email');
            $table->string('website');
            $table->string('age');
            $table->string('nationality');
            $table->string('province');
            $table->string('district');
            $table->string('sector');
            $table->string('cell');
            $table->string('workingexperience');
            $table->string('educationlevel');
            $table->string('areofinterest');
            $table->string('export');
            $table->string('attachyourpassport');
            $table->string('attachyourcv');
            $table->string('attachyourdiplomas');
            $table->string('drivinglicense');
            $table->string('Memberstatus');
            $table->string('user_id');
            $table->string('approved_date');
            $table->string('expiration_date');
            $table->string('bankslip');
            $table->string('identification')->nullable();
            $table->string('approval')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('JoinMember');
    }
}
